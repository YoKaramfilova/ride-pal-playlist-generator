export default (err, req, res, next) => {
  res.status(500).json({
    message: 'An unexpected error has occurred. Please retry your request later.',
  });
};

import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import passport from 'passport';
import swaggerUi from 'swagger-ui-express';
import swaggerJsdoc from 'swagger-jsdoc';

import jwtStrategy from './auth/strategy.js';

import authRouter from './routers/auth-router.js';
import adminRouter from './routers/admin-router.js';
import usersRouter from './routers/users-router.js';
import playlistsRouter from './routers/playlists-router.js';
import tracksRouter from './routers/tracks-router.js';
import genresRouter from './routers/genres-router.js';
import artistsRouter from './routers/artists-router.js';

import config from './config.js';
import errorHandler from './middlewares/error-handler.js';

const { env: { PORT }, swaggerOptions } = config;

const app = express();

// eslint-disable-next-line
const swaggerSpec = await swaggerJsdoc(swaggerOptions);

app.use('/images', express.static('images'));
app.use(cors());
app.use(helmet());
app.use(express.json());
passport.use(jwtStrategy);
app.use(passport.initialize());

app.use('/v1', authRouter);
app.use('/v1/admin', adminRouter);
app.use('/v1/users', usersRouter);
app.use('/v1/playlists', playlistsRouter);
app.use('/v1/tracks', tracksRouter);
app.use('/v1/genres', genresRouter);
app.use('/v1/artists', artistsRouter);
app.use('/v1/tracks', tracksRouter);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.all('*', (req, res) => {
  res.status(404).json({ message: 'Resource not found.' });
});

app.use(errorHandler);

app.listen(process.env.PORT || PORT, () => console.log(`Listening on ${PORT}...`));

export default {
  user: {
    username: 'Expected name to be string with length [8-20]. '
      + 'Could include alphanumeric characters, underscore and dot. '
      + 'Underscores and dots could not be at the beginning or end of the username, and can not be consecutive.',
    password: 'Expected password to be string with length [8-20]. '
      + 'Should contain at least one number, one lowercase letter, one capital letter and one special symbol.',
    newPassword: 'Expected password to be string with length [8-20]. '
      + 'Should contain at least one number, one lowercase letter, one capital letter and one special symbol.',
    oldPassword: 'Expected password to be string with length [8-20]. '
      + 'Should contain at least one number, one lowercase letter, one capital letter and one special symbol.',
    email: 'Expected email to be valid email with length of at least 7 symbols.',
  },
  playlist: {
    duration: 'Expected duration to be between 5 minutes and 24 hours.',
    genres: 'Expected genre mix to amount to 100%.',
    repeatArtists: 'Expected value on or no value',
    topTracks: 'Expected value on or no value',
    title: 'Expected title to be string with length [1-255].',
    imageURL: 'Expected imageURL to be a string up to 255 symbols',
  },
};

const VALID_MIMETYPES = ['image/png', 'image/jpeg'];

const PLAYTIME_OFF_BY = 5 * 60;

export default {
  VALID_MIMETYPES,
  PLAYTIME_OFF_BY,
};

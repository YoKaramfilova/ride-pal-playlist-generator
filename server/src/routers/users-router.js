import express from 'express';
import asyncHandler from 'express-async-handler';

import usersData from '../data/users-data.js';

import usersService from '../service/users-service.js';
import serviceErrors from '../common/service-errors.js';

import validateBody from '../middlewares/validate-body.js';
import authUser from '../middlewares/auth-user.js';
import transformBody from '../middlewares/transform-body.js';
import fileUpload from '../middlewares/file-upload.js';
import tokenLogoutUsers from '../middlewares/token-logout-users.js';

import createUserValidator from '../validators/create-user-validator.js';
import updateUserDetailsValidator from '../validators/update-user-details-validator.js';
import updateUserPasswordValidator from '../validators/update-user-password-validator.js';

const usersRouter = express.Router();

/**
 * @swagger
 * /users/{id}:
 *   get:
 *     description: Get user by id
 *     tags:
 *     - Get user by id
 *     security: [{
 *       bearerAuth: []
 *     }]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        description: Id of the user
 *        example: 1
 *     produces:
 *     - application/json
 *     responses:
 *       200:
 *         description: Returns a user object
 *         schema:
 *           type: object
 *           properties:
 *              id:
 *                type: integer
 *              username:
 *                type: string
 *              email:
 *                type: string
 *              avatar:
 *                type: string
 *              created_on:
 *                type: string
 *              role_id:
 *                type: integer
 *       404:
 *         description: User not found.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       401:
 *         description: Not authorized.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 */
usersRouter.get('/:id',
  authUser,
  asyncHandler(tokenLogoutUsers),
  asyncHandler(async (req, res) => {
    const { id } = req.params;

    const result = await usersService.getUserById(usersData)(id);

    if (result.error === serviceErrors.RECORD_NOT_FOUND) {
      res
        .status(404)
        .json({ message: `User with id ${id} not found.` });
    } else {
      res
        .status(200)
        .json(result.data);
    }
  }));

/**
 * @swagger
 * /users:
 *   post:
 *     description: Register
 *     tags:
 *     - Register
 *     consumes:
 *     - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Register'
 *     produces:
 *       - application/json
 *     responses:
 *       201:
 *         description: Returns user generated id and username.
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: string
 *             username:
 *               type: string
 *       409:
 *         description: Username or email already exist.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 */
usersRouter.post('/',
  transformBody(createUserValidator),
  validateBody('user', createUserValidator),
  asyncHandler(async (req, res) => {
    const result = await usersService.createUser(usersData)(req.body);

    if (result.error === serviceErrors.DUPLICATE_RECORD) {
      res
        .status(409)
        .json({ message: 'Username or email already exist.' });
    } else {
      res
        .status(201)
        .json(result.data);
    }
  }));

/**
 * @swagger
 * /users/{id}:
 *   put:
 *     description: Update user details by user id
 *     tags:
 *     - Update user details by user id
 *     security: [{
 *       bearerAuth: []
 *     }]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        description: Id of the user
 *        example: 20
 *     consumes:
 *       multipart/form-data
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/UpdateUserDetails'
 *           encoding:
 *             image:
 *               contentType: image/png, image/jpeg
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returns a user object
 *         schema:
 *           type: object
 *           properties:
 *              id:
 *                type: integer
 *              username:
 *                type: string
 *              email:
 *                type: string
 *              avatar:
 *                type: string
 *              created_on:
 *                type: string
 *              role_id:
 *                type: integer
 *       403:
 *         description: Not allowed to update user.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       400:
 *         description: Invalid image format.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       409:
 *         description: Username or email already exist.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       401:
 *         description: Not authorized.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 */
usersRouter.put('/:id',
  authUser,
  asyncHandler(tokenLogoutUsers),
  fileUpload,
  transformBody(updateUserDetailsValidator),
  validateBody('user', updateUserDetailsValidator),
  asyncHandler(async (req, res) => {
    const result = await usersService
      .updateUserDetails(usersData)(+req.params.id, req.body, req.file, req.user);

    if (result.error === serviceErrors.OPERATION_NOT_PERMITTED) {
      res
        .status(403)
        .json({ message: 'Not allowed to update this user.' });
    } else if (result.error === serviceErrors.DUPLICATE_RECORD) {
      res
        .status(409)
        .json({ message: 'Username or email already exist.' });
    } else if (result.error === serviceErrors.INVALID_REQUEST) {
      res
        .status(400)
        .json({ message: 'Please add an image in .png, .jpg and .jpeg format to upload.' });
    } else {
      res
        .status(200)
        .json(result.data);
    }
  }));

/**
 * @swagger
 * /users/{id}:
 *   patch:
 *     description: Change password by user id
 *     tags:
 *     - Change password by user id
 *     security: [{
 *       bearerAuth: []
 *     }]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        description: Id of the user
 *        example: 1
 *     consumes:
 *     - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UpdateUserPassword'
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returns a user object
 *         schema:
 *           type: object
 *           properties:
 *              id:
 *                type: integer
 *              username:
 *                type: string
 *              email:
 *                type: string
 *              avatar:
 *                type: string
 *              created_on:
 *                type: string
 *              role_id:
 *                type: integer
 *       403:
 *         description: Not allowed to update user or wrong previous password.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       404:
 *         description: User not found.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       409:
 *         description: New password should be different from old password.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       401:
 *         description: Not authorized.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 */
usersRouter.patch('/:id',
  authUser,
  asyncHandler(tokenLogoutUsers),
  transformBody(updateUserPasswordValidator),
  validateBody('user', updateUserPasswordValidator),
  asyncHandler(async (req, res) => {
    const result = await usersService
      .changePassword(usersData)(+req.params.id, req.body, req.user);

    if (result.error === serviceErrors.RECORD_NOT_FOUND) {
      res
        .status(404)
        .json({ message: `User with id ${+req.params.id} not found.` });
    } else if (result.error === serviceErrors.OPERATION_NOT_PERMITTED) {
      res
        .status(403)
        .json({ message: 'Not allowed to update this user.' });
    } else if (result.error === serviceErrors.DUPLICATE_RECORD) {
      res
        .status(409)
        .json({ message: 'New password should be different from previous password.' });
    } else if (result.error === serviceErrors.INVALID_REQUEST) {
      res
        .status(403)
        .json({ message: 'Incorrect previous password.' });
    } else {
      res
        .status(200)
        .json(result.data);
    }
  }));

export default usersRouter;

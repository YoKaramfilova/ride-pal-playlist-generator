import express from 'express';
import asyncHandler from 'express-async-handler';

import tokenLogoutUsers from '../middlewares/token-logout-users.js';
import authUser from '../middlewares/auth-user.js';
import tracksService from '../service/tracks-service.js';
import tracksData from '../data/tracks-data.js';

const tracksRouter = express.Router();

/**
 * @swagger
 * /tracks/{playlistId}:
 *   get:
 *     description: Get all tracks by playlist id
 *     tags:
 *     - Get all tracks by playlist id
 *     parameters:
 *      - in: path
 *        name: playlistId
 *        schema:
 *          type: integer
 *        description: Playlist id to get tracks for.
 *        example: 1
 *      - in: query
 *        name: genreId
 *        schema:
 *          type: integer
 *        description: Genre id to filter tracks by.
 *        example: 1
 *      - in: query
 *        name: artistId
 *        schema:
 *          type: integer
 *        description: Artist id to filter tracks by.
 *        example: 1
 *     produces:
 *     - application/json
 *     responses:
 *       200:
 *         description: Returns an array of tracks, as well as total duration and average rank.
 *         schema:
 *           type: object
 *           properties:
 *             duration:
 *               type: integer
 *             rank:
 *               type: integer
 *             data:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                    id:
 *                      type: integer
 *                    deezerId:
 *                      type: integer
 *                    trackName:
 *                      type: string
 *                    genreId:
 *                      type: integer
 *                    preview:
 *                      type: string
 *                    duration:
 *                      type: integer
 *                    rank:
 *                      type: integer
 *                    playlistTitle:
 *                      type: string
 *                    artist:
 *                      type: string
 *                    artistId:
 *                      type: integer
 *                    genre:
 *                      type: string
 *                    album:
 *                      type: string
 *                    cover:
 *                      type: string
 *       401:
 *         description: Not authorized.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 */
tracksRouter.get('/:playlistId',
  async (req, res) => {
    const { playlistId } = req.params;

    const result = await tracksService.getAllByPlaylist(tracksData)(+playlistId, req.query);

    res
      .status(200)
      .json(result);
  });

export default tracksRouter;

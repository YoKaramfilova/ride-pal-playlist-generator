import express from 'express';
import asyncHandler from 'express-async-handler';

import artistsData from '../data/artists-data.js';
import artistsService from '../service/artists-service.js';

const artistsRouter = express.Router();

/**
 * @swagger
 * /artists:
 *   get:
 *     description: Get all artists
 *     tags:
 *     - Get all artists
 *     produces:
 *     - application/json
 *     responses:
 *       200:
 *         description: Returns an array of artist objects.
 *         schema:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *                id:
 *                  type: integer
 *                deezer_id:
 *                  type: integer
 *                name:
 *                  type: string
 *                picture:
 *                  type: string
 *                tracklist:
 *                  type: string
 *                genre_id:
 *                  type: integer
 *                genre_deezer_id:
 *                  type: integer
 */
artistsRouter.get('/',
  asyncHandler(async (req, res) => {
    res
      .status(200)
      .json(await artistsService.getAllArtists(artistsData)());
  }));

export default artistsRouter;

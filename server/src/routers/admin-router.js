import express from 'express';
import asyncHandler from 'express-async-handler';

import usersData from '../data/users-data.js';

import authUser from '../middlewares/auth-user.js';
import validateUserRole from '../middlewares/validate-role.js';
import tokenLogoutUsers from '../middlewares/token-logout-users.js';

import usersService from '../service/users-service.js';
import adminService from '../service/admin-service.js';
import serviceErrors from '../common/service-errors.js';

import validateBody from '../middlewares/validate-body.js';
import transformBody from '../middlewares/transform-body.js';
import updateUserValidator from '../validators/update-user-validator.js';

const adminRouter = express.Router();

/**
 * @swagger
 * /admin/users:
 *   get:
 *     description: Get all users
 *     tags:
 *     - Get all users
 *     security: [{
 *       bearerAuth: []
 *     }]
 *     parameters:
 *      - in: query
 *        name: offset
 *        schema:
 *          type: integer
 *        description: The number of items to skip before starting to collect the result set
 *        example: 0
 *      - in: query
 *        name: limit
 *        schema:
 *          type: integer
 *        description: The numbers of items to return
 *        example: 10
 *      - in: query
 *        name: sortBy
 *        schema:
 *          type: string
 *        description: Parameter to sort results by and direction of the sort
 *        example: username.desc
 *      - in: query
 *        name: role
 *        schema:
 *          type: string
 *        description: User role to filter by
 *        example: Admin
 *      - in: query
 *        name: playlistsNumber
 *        schema:
 *          type: string
 *        description: Filter by count of user generated playlists (more or less than 10)
 *        example: Less than 10
 *     produces:
 *     - application/json
 *     responses:
 *       200:
 *         description: Returns an array of users and total user count without filters applied.
 *         schema:
 *           type: object
 *           properties:
 *             total:
 *               type: integer
 *             data:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                    id:
 *                      type: integer
 *                    username:
 *                      type: string
 *                    email:
 *                      type: string
 *                    avatar:
 *                      type: string
 *                    created_on:
 *                      type: string
 *                    role_id:
 *                      type: integer
 *                    role:
 *                      type: string
 *                    numberOfPlaylists:
 *                      type: integer
 *                    total:
 *                      type: integer
 *       401:
 *         description: Not authorized.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       403:
 *         description: Resource forbidden.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 */
adminRouter.get('/users',
  authUser,
  asyncHandler(tokenLogoutUsers),
  validateUserRole('admin'),
  asyncHandler(async (req, res) => {
    res
      .status(200)
      .json(await usersService.getAllUsers(usersData)(req.query));
  }));

/**
 * @swagger
 * /admin/users/{id}:
 *   delete:
 *     description: Delete user by id
 *     tags:
 *     - Delete user by id
 *     security: [{
 *       bearerAuth: []
 *     }]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        description: Id of the user
 *        example: 5
 *     produces:
 *     - application/json
 *     responses:
 *       200:
 *         description: Returns a user object
 *         schema:
 *           type: object
 *           properties:
 *              id:
 *                type: integer
 *              username:
 *                type: string
 *              email:
 *                type: string
 *              avatar:
 *                type: string
 *              created_on:
 *                type: string
 *              role_id:
 *                type: integer
 *       404:
 *         description: User not found.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       401:
 *         description: Not authorized.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 */
adminRouter.delete('/users/:id',
  authUser,
  asyncHandler(tokenLogoutUsers),
  validateUserRole('admin'),
  asyncHandler(async (req, res) => {
    const result = await usersService.removeUser(usersData)(+req.params.id);

    if (result.error === serviceErrors.RECORD_NOT_FOUND) {
      res
        .status(404)
        .json({ message: `User with id ${+req.params.id} not found.` });
    } else {
      res
        .status(200)
        .json(result.data);
    }
  }));

/**
 * @swagger
 * /admin/users/{id}:
 *   put:
 *     description: Update user by user id
 *     tags:
 *     - Update user by user id
 *     security: [{
 *       bearerAuth: []
 *     }]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: integer
 *        description: Id of the user
 *        example: 10
 *     consumes:
 *     - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UpdateUserByAdmin'
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returns a user object
 *         schema:
 *           type: object
 *           properties:
 *              id:
 *                type: integer
 *              username:
 *                type: string
 *              email:
 *                type: string
 *              avatar:
 *                type: string
 *              created_on:
 *                type: string
 *              role_id:
 *                type: integer
 *       403:
 *         description: Not allowed to update user.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       409:
 *         description: Username or email already exist.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       401:
 *         description: Not authorized.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 */
adminRouter.put('/users/:id',
  authUser,
  asyncHandler(tokenLogoutUsers),
  validateUserRole('admin'),
  transformBody(updateUserValidator),
  validateBody('user', updateUserValidator),
  asyncHandler(async (req, res) => {
    const result = await adminService.updateUser(usersData)(+req.params.id, req.body, req.user);

    if (result.error === serviceErrors.OPERATION_NOT_PERMITTED) {
      res
        .status(403)
        .json({ message: 'Not allowed to update this user.' });
    } else if (result.error === serviceErrors.DUPLICATE_RECORD) {
      res
        .status(409)
        .json({ message: 'Username or email already exist.' });
    } else {
      res
        .status(200)
        .json(result.data);
    }
  }));

export default adminRouter;

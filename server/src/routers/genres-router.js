import express from 'express';
import asyncHandler from 'express-async-handler';

import genresData from '../data/genres-data.js';
import genresService from '../service/genres-service.js';

const genresRouter = express.Router();

/**
 * @swagger
 * /genres:
 *   get:
 *     description: Get all music genres
 *     tags:
 *     - Get all music genres
 *     produces:
 *     - application/json
 *     responses:
 *       200:
 *         description: Returns an array of genre objects.
 *         schema:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *                id:
 *                  type: integer
 *                deezer_id:
 *                  type: integer
 *                name:
 *                  type: string
 *                picture:
 *                  type: string
 */
genresRouter.get('/',
  asyncHandler(async (req, res) => {
    res
      .status(200)
      .json(await genresService.getAllGenres(genresData)());
  }));

export default genresRouter;

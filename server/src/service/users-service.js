import bcrypt from 'bcryptjs';
import serviceErrors from '../common/service-errors.js';
import constants from '../common/constants.js';

const getAllUsers = usersData => async (query) => await usersData.searchBy(query);

const getUserById = usersData => async (id) => {
  const user = await usersData.getBy('id', id);

  if (!user) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      data: null,
    };
  }

  return { error: null, data: user };
};

const createUser = (usersData) => async (user) => {
  const { username, email, password } = user;

  const existingName = await usersData.getBy('username', username);

  const existingEmail = await usersData.getBy('email', email);

  if (existingName || existingEmail) {
    return {
      error: serviceErrors.DUPLICATE_RECORD,
      data: null,
    };
  }

  const cryptedPassword = await bcrypt.hash(password, 10);

  return {
    error: null,
    data: await usersData.create({
      ...user,
      password: cryptedPassword,
    }),
  };
};

const updateUserDetails = (usersData) => async (id, data, file, user) => {
  const { username, email } = data;

  if (id !== user.id) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null,
    };
  }

  if (username) {
    const existingName = await usersData.getBy('username', username);

    if (existingName) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        data: null,
      };
    }

    await usersData.update('username', username, id);
  }

  if (email) {
    const existingEmail = await usersData.getBy('email', email);

    if (existingEmail) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        data: null,
      };
    }

    await usersData.update('email', email, id);
  }

  if (file) {
    if (!constants.VALID_MIMETYPES.includes(file.mimetype)) {
      return {
        error: serviceErrors.INVALID_REQUEST,
        data: null,
      };
    }

    await usersData.update('avatar', file.filename, id);
  }

  return {
    error: null,
    data: await usersData.getBy('id', id),
  };
};

const changePassword = (usersData) => async (id, data, user) => {
  const { newPassword, oldPassword } = data;

  const userData = await usersData.getBy('id', id);

  if (!user) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      data: null,
    };
  }

  if (id !== user.id) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null,
    };
  }

  if (newPassword === oldPassword) {
    return {
      error: serviceErrors.DUPLICATE_RECORD,
      data: null,
    };
  }

  const auth = await usersData.getAuth(userData.username);

  if (!(await bcrypt.compare(oldPassword, auth.password))) {
    return {
      error: serviceErrors.INVALID_REQUEST,
      data: null,
    };
  }

  await usersData.update('password', await bcrypt.hash(newPassword, 10), id);

  return {
    error: null,
    data: await usersData.getBy('id', id),
  };
};

const signInUser = usersData => async (username, password) => {
  const user = await usersData.getAuth(username);

  if (!user || !(await bcrypt.compare(password, user.password))) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null,
    };
  }

  if (user.is_deleted) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: user,
  };
};

const removeUser = (usersData) => async (id) => {
  const user = await usersData.getBy('id', id);

  if (!user) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      data: null,
    };
  }

  await usersData.remove(id);

  return {
    error: null,
    data: user,
  };
};

export default {
  getAllUsers,
  getUserById,
  createUser,
  updateUserDetails,
  changePassword,
  signInUser,
  removeUser,
};

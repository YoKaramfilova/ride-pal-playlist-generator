import serviceErrors from '../common/service-errors.js';
import constants from '../common/constants.js';

const getAllPlaylists = playlistsData => async (query) => await playlistsData.getAll(query);

const getPlaylistById = playlistsData => async (id) => {
  const playlist = await playlistsData.getBy('id', id);

  if (!playlist) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      data: null,
    };
  }

  return { error: null, data: playlist };
};

const createPlaylist = playlistsData => async (title, imageFile, imageURL, userId) => {
  const existingPlaylist = await playlistsData.getBy('title', title);

  if (existingPlaylist) {
    return {
      error: serviceErrors.DUPLICATE_RECORD,
      data: null,
    };
  }

  let imageSource = imageFile || imageURL;

  if (imageSource === imageFile) {
    if (!constants.VALID_MIMETYPES.includes(imageFile.mimetype)) {
      return {
        error: serviceErrors.INVALID_REQUEST,
        data: null,
      };
    }

    imageSource = imageFile.filename;
  }

  const playlistId = await playlistsData.create(title, imageSource, userId);

  return {
    error: null,
    data: await playlistsData.getBy('id', playlistId),
  };
};

const updatePlaylistPlaytime = playlistsData => (
  async (playtime, playlistId) => (
    await playlistsData.update('playtime', playtime, playlistId)
  )
);

const updatePlaylistRank = playlistsData => (
  async (rank, playlistId) => (
    await playlistsData.update('rank', rank, playlistId)
  )
);

const removePlaylist = (playlistsData) => async (id) => {
  const playlist = await playlistsData.getBy('id', id);

  if (!playlist) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      data: null,
    };
  }

  await playlistsData.remove(id);

  return {
    error: null,
    data: playlist,
  };
};

const updatePlaylistDetails = (playlistsData) => async (id, data, file) => {
  const { title, imageURL } = data;

  if (title) {
    const existingTitle = await playlistsData.getBy('title', title);

    if (existingTitle) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        data: null,
      };
    }

    await playlistsData.update('title', title, id);
  }

  if (imageURL) {
    await playlistsData.update('picture', imageURL, id);
  }

  if (file) {
    if (!constants.VALID_MIMETYPES.includes(file.mimetype)) {
      return {
        error: serviceErrors.INVALID_REQUEST,
        data: null,
      };
    }

    await playlistsData.update('picture', file.filename, id);
  }

  return {
    error: null,
    data: await playlistsData.getBy('id', id),
  };
};

export default {
  getAllPlaylists,
  getPlaylistById,
  createPlaylist,
  updatePlaylistPlaytime,
  updatePlaylistRank,
  removePlaylist,
  updatePlaylistDetails,
};

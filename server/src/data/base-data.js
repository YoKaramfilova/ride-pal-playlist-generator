const createSortQuery = (sortBy) => {
  if (!sortBy) return '';
  if (Array.isArray(sortBy)) return `ORDER BY ${sortBy.map(el => `${el.split('.')[0]} ${el.split('.')[1].toUpperCase()}`).join(', ')}`;
  return `ORDER BY ${sortBy.split('.')[0]} ${sortBy.split('.')[1].toUpperCase()}`;
};

const createLimitQuery = (limit) => {
  if (!limit || Number.isNaN(limit)) return '';
  if (+limit < 0) limit = 2;
  return `LIMIT ${limit}`;
};

const createOffsetQuery = (offset) => {
  if (!offset || Number.isNaN(offset)) return '';
  if ((+offset) < 0) offset = 0;
  return `OFFSET ${offset}`;
};

const createFilterQuery = (filters) => {
  if (!filters) return '';
  return Object.keys(filters).map(key => {
    if (Array.isArray(filters[key])) {
      return `AND (${filters[key].map(element => `${key} LIKE '%${element}%'`).join(' OR ')})`;
    }
    return `AND ${key} LIKE '%${filters[key]}%'`;
  }).join(' ');
};

const createPlaytimeQuery = (playtime) => {
  if (!playtime) return '';

  if (Array.isArray(playtime)) {
    return `AND (${playtime.map(elem => {
      const [min, max] = elem.split('-');
      return `playtime >= ${min} AND playtime <= ${max}`;
    }).join(' OR ')})`;
  }

  const [min, max] = playtime.split('-');
  return `AND playtime >= ${min} AND playtime <= ${max}`;
};

const createUserIdQuery = (userId) => {
  if (!userId) return '';
  return `AND user_id = ${userId}`;
};
const createRoleIdQuery = (roleId) => {
  if (!roleId) return '';
  return `AND role_id = ${roleId}`;
};

const createTopPlaylistsQuery = (topPlaylists) => {
  if (!topPlaylists) return '';
  return 'AND rank > (SELECT ROUND(AVG(rank)) FROM playlists)';
};

const createPlaylistsNumberQuery = (playlistsNumber) => {
  if (!playlistsNumber) return '';
  if (playlistsNumber.includes('Less')) return 'HAVING numberOfPlaylists < 10';
  if (playlistsNumber.includes('At')) return 'HAVING numberOfPlaylists > 0';
  return 'HAVING numberOfPlaylists >= 10';
};

const parseQueryParams = (query = {}) => {
  const {
    sortBy, limit, offset, playtime, userId, topPlaylists, roleId, playlistsNumber, ...filters
  } = query;

  return {
    sortBy: createSortQuery(sortBy),
    limit: createLimitQuery(limit),
    offset: createOffsetQuery(offset),
    filters: createFilterQuery(filters),
    playtime: createPlaytimeQuery(playtime),
    userId: createUserIdQuery(userId),
    roleId: createRoleIdQuery(roleId),
    topPlaylists: createTopPlaylistsQuery(topPlaylists),
    playlistsNumber: createPlaylistsNumberQuery(playlistsNumber),
  };
};

const searchBy = (query) => {
  const {
    limit, offset, sortBy, filters, playtime, userId, topPlaylists, roleId,
    playlistsNumber,
  } = parseQueryParams(query);
  const sql = `
    ${filters}
    ${playtime}
    ${userId}
    ${roleId}
    ${topPlaylists}
    ${sortBy}
    ${limit}
    ${offset}
    ${playlistsNumber}`;
  return sql;
};

export default {
  searchBy,
};

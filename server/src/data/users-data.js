import pool from './pool.js';
import data from './base-data.js';

const searchBy = async (query) => {
  const { role, playlistsNumber } = query;

  const roleQuery = {};
  const playlistsNumberQuery = {};
  if (role) roleQuery.role = role;
  if (playlistsNumber) playlistsNumberQuery.playlistsNumber = playlistsNumber;

  const sql = `
    SELECT 
      u.id,
      u.username,
      u.email,
      u.avatar,
      u.created_on,
      u.role_id,
      r.role,
      count(case when p.is_deleted=0 then 1 end) as numberOfPlaylists
    FROM users u
    LEFT JOIN playlists p ON u.id = p.user_id
    JOIN roles r ON u.role_id = r.id
    WHERE u.is_deleted = 0 ${data.searchBy(roleQuery)}
    group by u.id ${data.searchBy(playlistsNumberQuery)}`;
  const result = await pool.query(sql);

  return {
    total: result[0] ? result[0].total : 0,
    data: result,
  };
};

const getAuth = async (username) => {
  const sql = `
    SELECT 
      u.id, 
      u.username,
      u.password,
      u.role_id,
      r.role,
      u.is_deleted
    FROM users u
    JOIN roles r ON u.role_id = r.id
    WHERE u.username = ?;
    `;

  return (await pool.query(sql, [username]))[0];
};

const getBy = async (column, value) => {
  const sql = `
    SELECT 
      u.id,
      u.username,
      u.email,
      u.avatar,
      u.created_on,
      u.role_id
    FROM users u
    WHERE u.${column} = ?
    AND u.is_deleted = 0;
    `;
  const result = await pool.query(sql, [value]);

  return result[0];
};

const create = async ({ username, email, password }) => {
  const sql = `
    INSERT INTO users (username, email, password)
    VALUES (?, ?, ?);
    `;
  const result = await pool.query(sql, [username, email, password]);
  return {
    id: result.insertId,
    username,
  };
};

const update = async (column, value, id) => {
  const sql = `
    UPDATE users SET
    ${column} = ?
    WHERE id = ?;
    `;

  return await pool.query(sql, [value, id]);
};

const remove = async (id) => {
  const sql = `
    UPDATE users
    SET is_deleted = 1
    WHERE id = ?
    `;
  return await pool.query(sql, [id]);
};

export default {
  getAuth,
  getBy,
  searchBy,
  create,
  update,
  remove,
};

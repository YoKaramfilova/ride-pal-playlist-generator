import pool from './pool.js';

const getAllByPlaylist = async (value, query) => {
  const { genreId, artistId } = query;
  let queryParams = '';
  if (genreId) queryParams = `AND t.genre_id = ${genreId}`;
  if (artistId) queryParams = `AND a.id = ${artistId}`;

  const sql = ` 
  SELECT
    t.id,
    t.title as trackName,
    t.genre_id as genreId,
    t.deezer_id as deezerId,
    t.preview,
    t.duration,
    t.rank,
    al.cover,
    p.title as playlistTitle,
    a.name as artist,
    a.id as artistId,
    g.name as genre,
    al.title as album
  FROM playlists_has_tracks pht
  JOIN playlists p ON pht.playlist_id = p.id
  JOIN tracks t ON pht.track_id = t.id
  JOIN artists a ON t.artist_id = a.id
  JOIN genres g ON t.genre_id = g.id
  JOIN albums al ON t.album_id = al.id
  WHERE p.id = ? 
  ${queryParams};`;

  const result = await pool.query(sql, [value]);

  if (result) {
    const duration = result.map(t => t.duration).reduce((acc, cur) => acc + cur);
    const rank = Math.round(result.map(t => t.rank)
      .reduce((acc, cur) => acc + cur) / result.length);
    return {
      duration,
      rank,
      data: result,
    };
  }

  return result;
};

const getRandomizedTracksByGenre = async (genreId, topTracks) => {
  const sql = `
    SELECT *
    FROM tracks
    WHERE genre_id = ?
    ${topTracks
    ? 'AND rank > (SELECT ROUND(AVG(rank)) FROM tracks)'
    : ''}
    ORDER BY RAND();`;

  return await pool.query(sql, [genreId]);
};

const addTrackToPlaylist = async (playlistId, trackId, trackDeezerId) => {
  const sql = `
    INSERT INTO playlists_has_tracks (playlist_id, track_id, track_deezer_id)
    VALUES (?, ?, ?)`;

  return await pool.query(sql, [playlistId, trackId, trackDeezerId]);
};

export default {
  getAllByPlaylist,
  getRandomizedTracksByGenre,
  addTrackToPlaylist,
};

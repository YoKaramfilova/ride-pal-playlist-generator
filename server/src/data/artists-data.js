import pool from './pool.js';

const getAll = async () => {
  const sql = `
    SELECT * FROM artists
    `;

  return await pool.query(sql);
};

export default {
  getAll,
};

import pool from './pool.js';
import data from './base-data.js';

const getAll = async (query) => {
  const { title, playtime, genres, userId, topPlaylists } = query;
  const countQuery = {};

  if (genres) countQuery.genres = genres;
  if (title) countQuery.title = title;
  if (playtime) countQuery.playtime = playtime;
  if (userId) countQuery.userId = userId;
  if (topPlaylists) countQuery.topPlaylists = topPlaylists;

  const sql = `
  SELECT 
    p.id,
    p.title,
    p.playtime,
    p.rank,
    p.picture,
    p.created_on,
    p.user_id,
    p.is_deleted,
    gp.genre_id,
    u.username,
    GROUP_CONCAT(g.name) as genres,
    (SELECT COUNT(*) 
      FROM (SELECT
        p.id,
        p.title,
        p.playtime,
        p.rank,
        p.picture,
        p.created_on,
        p.user_id,
        p.is_deleted,
        gp.genre_id,
        u.username,
        GROUP_CONCAT(g.name) as genres
      FROM playlists p
      JOIN genres_has_playlists as gp ON p.id = gp.playlist_id
      JOIN genres g ON gp.genre_id = g.id
      JOIN users u ON p.user_id = u.id
      GROUP BY p.title
      HAVING p.is_deleted = 0
      ${data.searchBy(countQuery)}
      ) as temp) as total
  FROM playlists p
  JOIN genres_has_playlists as gp ON p.id = gp.playlist_id
  JOIN genres g ON gp.genre_id = g.id
  JOIN users u ON p.user_id = u.id
  GROUP BY p.title
  HAVING p.is_deleted = 0
  ${data.searchBy(query)}`;

  const result = await pool.query(sql);

  return {
    total: result[0] ? result[0].total : 0,
    data: result,
  };
};

const getBy = async (col, value) => {
  const sql = `
  SELECT
    p.id,
    p.title,
    p.playtime,
    p.rank,
    p.picture,
    p.created_on as createdOn,
    p.user_id as userId,
    p.is_deleted,
    u.username,
    GROUP_CONCAT(g.name) as genres
  FROM playlists AS p
  LEFT OUTER JOIN genres_has_playlists AS gp ON p.id = gp.playlist_id
  LEFT OUTER JOIN genres AS g ON gp.genre_id = g.id
  LEFT OUTER JOIN users AS u ON p.user_id = u.id
  GROUP BY p.title
  HAVING p.${col} = ?
  AND p.is_deleted = 0`;

  const result = await pool.query(sql, [value]);
  return result[0];
};

const create = async (title, picture, userId) => {
  const sql = `
    INSERT INTO playlists (title, picture, user_id)
    VALUES (?, ?, ?)`;

  const result = await pool.query(sql, [title, picture, userId]);

  return result.insertId;
};

const update = async (column, value, id) => {
  const sql = `
    UPDATE playlists SET
    ${column} = ?
    WHERE id = ?;
    `;

  return await pool.query(sql, [value, id]);
};

const remove = async (id) => {
  const sql = `
    UPDATE playlists
    SET is_deleted = 1
    WHERE id = ?
    `;
  return await pool.query(sql, [id]);
};

export default {
  getAll,
  getBy,
  create,
  update,
  remove,
};

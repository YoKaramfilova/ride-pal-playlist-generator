export default {
  START_AND_END_POINTS: {
    label: 'Add start and end points',
    step: 0,
  },
  DURATION: {
    label: 'Confirm trip duration',
    step: 1,
  },
  GENRES: {
    label: 'Adjust genre mix',
    step: 2,
  },
  PREFERENCES: {
    label: 'Adjust playlist preferences',
    step: 3,
  },
  PERSONALIZATION: {
    label: 'Add playlist name and cover',
    step: 4,
  },
  CONFIRMATION: {
    label: 'Generate playlist',
    step: 5,
  },
};

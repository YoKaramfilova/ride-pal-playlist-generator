const ROWS_PER_PAGE = [10, 20, 30];

export default {
  ROWS_PER_PAGE,
};

/* eslint-disable no-useless-escape */

const PLAYLIST_FORM_CONTROLS = {
  duration: {
    name: 'duration',
    placeholder: 'Duration',
    value: '',
    validation: {
      required: true,
      minDuration: 5 * 60,
      maxDuration: 24 * 60 * 60,
      regex: /^(?=.*[1-9])([0-9][0-9]:[0-5][0-9]:[0-5][0-9])$/g,
    },
    valid: false,
    touched: false,
  },
  genres: {
    value: {},
    validation: {
      valuesTotal: 100,
    },
    valid: false,
    touched: false,
  },
  repeatArtists: {
    placeholder: 'Allow tracks from the same artists',
    value: 'on',
    validation: {
      required: false,
    },
    valid: true,
    touched: true,
  },
  topTracks: {
    placeholder: 'Use only top tracks',
    value: '',
    validation: {
      required: false,
    },
    valid: true,
    touched: true,
  },
  title: {
    name: 'title',
    placeholder: 'Playlist title',
    value: '',
    validation: {
      required: true,
      minLength: 1,
      maxLength: 255,
    },
    valid: false,
    touched: false,
  },
  picture: {
    name: 'picture',
    value: '',
    files: null,
    imageURL: '',
    preview: '/images/default-playlist.png',
    validation: {
      required: true,
      validFileTypes: ['image/png', 'image/jpeg']
    },
    valid: false,
    touched: false,
  },
};

const EDIT_PLAYLIST_FORM_CONTROLS = {
  title: {
    name: 'title',
    placeholder: 'Playlist title',
    value: '',
    validation: {
      required: true,
      minLength: 1,
      maxLength: 255,
    },
    valid: false,
    touched: false,
  },
  picture: {
    name: 'picture',
    value: '',
    files: null,
    imageURL: '',
    preview: '/images/default-playlist.png',
    validation: {
      validFileTypes: ['image/png', 'image/jpeg']
    },
    valid: true,
    touched: false,
  },
};

const REGISTER_FORM_CONTROLS = {
  username: {
    name: 'username',
    placeholder: 'Username',
    value: '',
    validation: {
      required: true,
      regex: /^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/g,
    },
    valid: false,
    touched: false,
  },
  email: {
    name: 'email',
    placeholder: 'Email',
    value: '',
    validation: {
      required: true,
      minLength: 7,
      regex: /^(?![_.-])(?!.*[_.-]{2})[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-z]+$/g,
    },
    valid: false,
    touched: false,
  },
  password: {
    name: 'password',
    placeholder: 'Password',
    value: '',
    validation: {
      required: true,
      regex: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?\/~_+\-=|\\]).{8,20}$/g,
    },
    valid: false,
    touched: false,
  },
  repeatPassword: {
    name: 'repeatPassword',
    placeholder: 'Confirm password',
    value: '',
    validation: {
      required: true,
      regex: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?\/~_+\-=|\\]).{8,20}$/g,
    },
    valid: false,
    touched: false,
  },
};

const LOGIN_FORM_CONTROLS = {
  username: {
    name: 'username',
    placeholder: 'Username',
    value: '',
    validation: {
      required: true,
    },
    valid: false,
    touched: false,
  },
  password: {
    name: 'password',
    placeholder: 'Password',
    value: '',
    validation: {
      required: true,
    },
    valid: false,
    touched: false,
  },
};

const USER_DETAILS_FORM_CONTROLS = {
  username: {
    name: 'username',
    placeholder: 'Username',
    value: '',
    validation: {
      required: true,
      regex: /^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/g,
    },
    valid: false,
    touched: false,
  },
  email: {
    name: 'email',
    placeholder: 'Email',
    value: '',
    validation: {
      required: true,
      minLength: 7,
      regex: /^(?![_.-])(?!.*[_.-]{2})[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-z]+$/g,
    },
    valid: false,
    touched: false,
  },
  avatar: {
    name: 'avatar',
    value: '',
    files: null,
    imageURL: '',
    preview: '/images/default-avatar.png',
    validation: {
      validFileTypes: ['image/png', 'image/jpeg']
    },
    valid: true,
    touched: false,
  },
};

const USER_PASSWORD_FORM_CONTROLS = {
  newPassword: {
    name: 'newPassword',
    placeholder: 'New password',
    value: '',
    validation: {
      required: true,
      regex: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?\/~_+\-=|\\]).{8,20}$/g,
    },
    valid: false,
    touched: false,
  },
  oldPassword: {
    name: 'oldPassword',
    placeholder: 'Previous password',
    value: '',
    validation: {
      required: true,
      regex: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?\/~_+\-=|\\]).{8,20}$/g,
    },
    valid: false,
    touched: false,
  },
  repeatPassword: {
    name: 'repeatPassword',
    placeholder: 'Confirm password',
    value: '',
    validation: {
      required: true,
      regex: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?\/~_+\-=|\\]).{8,20}$/g,
    },
    valid: false,
    touched: false,
  },
};

const USER_FORM_CONTROLS = {
  username: {
    name: 'username',
    placeholder: 'Username',
    value: '',
    validation: {
      regex: /^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/g,
    },
    valid: true,
    touched: false,
  },
  email: {
    name: 'email',
    placeholder: 'Email',
    value: '',
    validation: {
      minLength: 7,
      regex: /^(?![_.-])(?!.*[_.-]{2})[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-z]+$/g,
    },
    valid: true,
    touched: false,
  },
  password: {
    name: 'password',
    placeholder: 'Password',
    value: '',
    validation: {
      regex: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?\/~_+\-=|\\]).{8,20}$/g,
    },
    valid: true,
    touched: false,
  },
};

export default {
  PLAYLIST_FORM_CONTROLS,
  EDIT_PLAYLIST_FORM_CONTROLS,
  REGISTER_FORM_CONTROLS,
  LOGIN_FORM_CONTROLS,
  USER_FORM_CONTROLS,
  USER_DETAILS_FORM_CONTROLS,
  USER_PASSWORD_FORM_CONTROLS,
};

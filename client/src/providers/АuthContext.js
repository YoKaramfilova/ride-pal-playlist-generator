import { createContext, useState } from 'react';
import jwtDecode from 'jwt-decode';
import PropTypes from 'prop-types';

export const AuthContext = createContext({
  user: null,
  setUser: () => {},
  avatar: null,
  setAvatar: () => {},
});

export const getUser = () => {
  try {
    return jwtDecode(localStorage.getItem('token') || '');
  } catch (error) {
    return null;
  }
};

const AuthContextProvider = ({ children }) => {
  const [user, setUser] = useState(getUser());
  const [avatar, setAvatar] = useState('');

  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
        avatar,
        setAvatar
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

AuthContextProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
export default AuthContextProvider;

import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import { Duration } from 'luxon';
import PropTypes from 'prop-types';

const useStyles = makeStyles(() => ({
  card: {
    width: '100%',
    transition: 'transform 0.25s ease-in-out',
    '&:hover': { transform: 'scale3d(1.04, 1.04, 1)' },
  },
  media: {
    paddingTop: '50%',
  },
  bold: {
    fontWeight: 'bold',
  }
}));

const Playlist = ({
  id, title, playtime, rank, picture, genres
}) => {
  const classes = useStyles();

  return (
    <Card
      className={classes.card}
    >
      <CardMedia
        image={picture.includes('unsplash')
          ? picture
          : `/images/${picture}`}
        alt={title}
        className={classes.media}
        component={NavLink}
        to={`/playlists/${id}`}
      />
      <CardContent>
        <Typography
          gutterBottom
          variant="h6"
          color="secondary"
          className={classes.bold}
        >
          {title}
        </Typography>
        <Typography
          gutterBottom
          variant="subtitle1"
        >
          {genres.split(',').sort().join(' • ')}
        </Typography>
        <Grid
          container
          justify="space-between"
        >
          <Grid item>
            <Grid
              container
              alignItems="center"
              spacing={1}
            >
              <Grid item>
                <AccessTimeIcon
                  color="secondary"
                />
              </Grid>
              <Grid item>
                <Typography
                  variant="subtitle2"
                >
                  {Duration.fromMillis(playtime * 1000).toFormat('hh:mm:ss')}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              alignItems="center"
              spacing={1}
            >
              <Grid item>
                <EqualizerIcon
                  color="secondary"
                />
              </Grid>
              <Grid item>
                <Typography
                  variant="subtitle2"
                >
                  {rank}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

Playlist.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  playtime: PropTypes.number.isRequired,
  rank: PropTypes.number.isRequired,
  picture: PropTypes.string.isRequired,
  genres: PropTypes.string.isRequired,
};

export default Playlist;

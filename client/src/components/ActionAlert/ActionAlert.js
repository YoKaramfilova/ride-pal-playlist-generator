import Alert from '@material-ui/lab/Alert';
import { Snackbar } from '@material-ui/core';
import PropTypes from 'prop-types';

const ActionAlert = ({ notification, handleClose }) => (
  <Snackbar
    open={notification.isOpen}
    autoHideDuration={3000}
    onClose={handleClose}
    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
  >
    <Alert
      severity={notification.type}
      onClose={handleClose}
    >
      {notification.message}
    </Alert>
  </Snackbar>
);

ActionAlert.propTypes = {
  handleClose: PropTypes.func.isRequired,
  notification: PropTypes.shape({
    isOpen: PropTypes.bool.isRequired,
    type: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
  }).isRequired,
};

export default ActionAlert;

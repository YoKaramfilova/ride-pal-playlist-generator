import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import PropTypes from 'prop-types';

const AdminTableDense = ({ setDense, dense }) => {
  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };
  return (
    <FormControlLabel
      control={<Switch checked={dense} onChange={handleChangeDense} />}
      label="Dense padding"
    />
  );
};
AdminTableDense.propTypes = {
  dense: PropTypes.bool.isRequired,
  setDense: PropTypes.func.isRequired,
};
export default AdminTableDense;

import TablePagination from '@material-ui/core/TablePagination';
import PropTypes from 'prop-types';

const AdminTablePagination = ({
  handleChangeRowsPerPage, count, rowsPerPage,
  rowsPerPageOptions, page,
  onChangePage
}) => (
  <TablePagination
    component="div"
    rowsPerPageOptions={rowsPerPageOptions}
    count={count}
    rowsPerPage={rowsPerPage}
    page={page}
    onChangePage={onChangePage}
    onChangeRowsPerPage={handleChangeRowsPerPage}
  />
);
AdminTablePagination.propTypes = {
  handleChangeRowsPerPage: PropTypes.func.isRequired,
  count: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  rowsPerPageOptions: PropTypes.arrayOf(PropTypes.number).isRequired,
  page: PropTypes.number.isRequired,
};
export default AdminTablePagination;

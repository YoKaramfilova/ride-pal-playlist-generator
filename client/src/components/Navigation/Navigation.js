import { useState, useContext, useEffect } from 'react';
import { useHistory, NavLink } from 'react-router-dom';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import MenuIcon from '@material-ui/icons/Menu';
import MoreIcon from '@material-ui/icons/MoreVert';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import { AuthContext } from 'providers/АuthContext';
import userService from 'services/user';
import AdminMenu from 'components/Navigation/AdminMenu';
import UserProfileMenu from 'components/Navigation/UserProfileMenu';
import AuthMenu from 'components/Navigation/AuthMenu';
import MobileNav from 'components/Navigation/MobileNav';

const useStyles = makeStyles((theme) => ({
  itemContainer: {
    width: 'auto',
  },
  navLink: {
    color: theme.palette.secondary.light,
    textDecoration: 'none',
    '&:hover': {
      color: theme.palette.secondary.light,
    },
    fontSize: theme.spacing(2),
  },
  navLinkActive: {
    color: theme.palette.primary.light,
  },
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
  },
  logo: {
    width: theme.spacing(6),
    height: theme.spacing(6),
    marginRight: theme.spacing(1),
  },
}));

const Navigation = () => {
  const classes = useStyles();
  const { user, avatar, setAvatar } = useContext(AuthContext);
  const history = useHistory();
  const theme = useTheme();
  const isSmall = useMediaQuery(theme.breakpoints.down('sm'));
  const isXSmall = useMediaQuery(theme.breakpoints.down('xs'));

  const [mobileNavAnchorEl, setMobileNavAnchorEl] = useState(null);
  const [authMenuAnchorEl, setAuthMenuAnchorEl] = useState(null);
  const [adminMenuAnchorEl, setAdminMenuAnchorEl] = useState(null);
  const [userMenuAnchorEl, setUserMenuAnchorEl] = useState(null);

  const isMobileNavOpen = Boolean(mobileNavAnchorEl);
  const isAdminMenuOpen = Boolean(adminMenuAnchorEl);
  const isUserMenuOpen = Boolean(userMenuAnchorEl);
  const isAuthMenuOpen = Boolean(authMenuAnchorEl);

  useEffect(() => {
    if (user) {
      userService.getUser(+user.id)
        .then((result) => {
          setAvatar(result.avatar);
        })
        .catch(() => history.push('/500'));
    }
  }, [user]);

  const handleMobileNavOpen = (event) => {
    setMobileNavAnchorEl(event.currentTarget);
  };

  const handleAuthMenuOpen = (event) => {
    setAuthMenuAnchorEl(event.currentTarget);
  };

  const handleAdminMenuOpen = (event) => {
    event.preventDefault();
    setAdminMenuAnchorEl(event.currentTarget);
  };

  const handleUserMenuOpen = (event) => {
    event.preventDefault();
    setUserMenuAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setMobileNavAnchorEl(null);
    setAdminMenuAnchorEl(null);
    setUserMenuAnchorEl(null);
    setAuthMenuAnchorEl(null);
  };

  return (
    <>
      <AppBar position="static">
        <Toolbar
          component={Grid}
          container
          spacing={2}
          wrap='nowrap'
          justify='space-between'
        >
          <Grid item hidden={!isSmall}>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleMobileNavOpen}
            >
              <MenuIcon />
            </IconButton>
            <MobileNav
              handleMenuClose={handleMenuClose}
              mobileNavAnchorEl={mobileNavAnchorEl}
              isMobileNavOpen={isMobileNavOpen}
            />
          </Grid>

          <Grid item hidden={isSmall}>
            <Grid
              container
              spacing={1}
              className={classes.itemContainer}
              alignItems='center'
            >
              <Grid item>
                <img
                  src='/images/logo-white.png'
                  alt="logo"
                  className={classes.logo}
                />
              </Grid>
              <Grid item>
                <NavLink
                  to='/home'
                  className={classes.navLink}
                  activeClassName={classes.navLinkActive}
                >
                  Home
                </NavLink>
              </Grid>
              <Grid item />
              <Grid item>
                <NavLink
                  to='/playlists'
                  className={classes.navLink}
                  activeClassName={classes.navLinkActive}
                >
                  Playlists
                </NavLink>
              </Grid>
              <Grid item />
              {user && user.role === 'admin' && (
                <>
                  <Grid item>
                    <NavLink
                      to='#'
                      className={classes.navLink}
                      activeClassName={classes.navLinkActive}
                      isActive={(match, location) => location.pathname.includes('admin')}
                      onClick={handleAdminMenuOpen}
                    >
                      Admin
                    </NavLink>
                    <AdminMenu
                      adminMenuAnchorEl={adminMenuAnchorEl}
                      handleMenuClose={handleMenuClose}
                      isAdminMenuOpen={isAdminMenuOpen}
                    />
                  </Grid>
                </>
              )}
              <Grid item>
                <Button
                  variant="contained"
                  color="secondary"
                  component={NavLink}
                  to="/create-playlist"
                >
                  Create Playlist
                </Button>
              </Grid>
            </Grid>
          </Grid>
          <Grid item hidden={!isXSmall}>
            {user ? (
              <>
                <Avatar
                  alt={user.username}
                  src={`/images/${avatar}`}
                  className={classes.avatar}
                  onClick={handleUserMenuOpen}
                />
                <UserProfileMenu
                  userMenuAnchorEl={userMenuAnchorEl}
                  handleMenuClose={handleMenuClose}
                  isUserMenuOpen={isUserMenuOpen}
                />
              </>
            ) : (
              <>
                <IconButton
                  edge='end'
                  onClick={handleAuthMenuOpen}
                  color="inherit"
                >
                  <MoreIcon />
                </IconButton>
                <AuthMenu
                  authMenuAnchorEl={authMenuAnchorEl}
                  isAuthMenuOpen={isAuthMenuOpen}
                  handleMenuClose={handleMenuClose}
                />
              </>
            )}
          </Grid>
          <Grid item hidden={isXSmall}>
            <Grid
              container
              spacing={1}
              className={classes.itemContainer}
              alignItems='center'
            >
              {user && (
                <Grid item>
                  <NavLink
                    to='#'
                    className={classes.navLink}
                    activeClassName={classes.navLinkActive}
                    onClick={handleUserMenuOpen}
                  >
                    <Avatar
                      alt={user.username}
                      src={`/images/${avatar}`}
                      className={classes.avatar}
                    />
                  </NavLink>
                  <UserProfileMenu
                    userMenuAnchorEl={userMenuAnchorEl}
                    handleMenuClose={handleMenuClose}
                    isUserMenuOpen={isUserMenuOpen}
                  />
                </Grid>
              )}
              {!user && (
                <>
                  <Grid item>
                    <NavLink
                      to='/login'
                      className={classes.navLink}
                      activeClassName={classes.navLinkActive}
                    >
                      Login
                    </NavLink>
                  </Grid>
                  <Grid item />
                  <Grid item>
                    <NavLink
                      to='/register'
                      className={classes.navLink}
                      activeClassName={classes.navLinkActive}
                    >
                      Register
                    </NavLink>
                  </Grid>
                </>
              )}
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Navigation;

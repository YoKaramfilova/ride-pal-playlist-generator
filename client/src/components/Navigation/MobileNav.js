import { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Divider from '@material-ui/core/Divider';
import { AuthContext } from 'providers/АuthContext';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  navButton: {
    marginRight: theme.spacing(0.5),
    marginLeft: theme.spacing(0.5),
  },
  navLink: {
    color: theme.palette.secondary.light,
    textDecoration: 'none',
    '&:hover': {
      color: theme.palette.secondary.light,
    },
    fontSize: theme.spacing(2),
  },
}));

const MobileNav = ({
  handleMenuClose, mobileNavAnchorEl, isMobileNavOpen
}) => {
  const classes = useStyles();
  const auth = useContext(AuthContext);

  return (
    <Menu
      anchorEl={mobileNavAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'left' }}
      open={isMobileNavOpen}
      onClose={handleMenuClose}
    >
      <MenuItem
        className={classes.navButton}
        component={NavLink}
        to="/home"
        onClick={handleMenuClose}
      >
        Home
      </MenuItem>
      <MenuItem
        className={classes.navButton}
        component={NavLink}
        to="/playlists"
        onClick={handleMenuClose}
      >
        Playlists
      </MenuItem>
      <MenuItem
        className={classes.navButton}
        component={NavLink}
        to="/create-playlist"
        onClick={handleMenuClose}
      >
        Create Playlist
      </MenuItem>
      {auth.user && auth.user.role === 'admin' && (
        <Divider />
      )}
      {auth.user && auth.user.role === 'admin' && (
        <MenuItem
          className={classes.navButton}
          component={NavLink}
          to="/admin/playlists"
          onClick={handleMenuClose}
        >
          Admin Playlists
        </MenuItem>
      )}
      {auth.user && auth.user.role === 'admin' && (
        <MenuItem
          className={classes.navButton}
          component={NavLink}
          to="/admin/users"
          onClick={handleMenuClose}
        >
          Admin Users
        </MenuItem>
      )}
    </Menu>
  );
};

MobileNav.propTypes = {
  mobileNavAnchorEl: PropTypes.instanceOf(Element),
  isMobileNavOpen: PropTypes.bool.isRequired,
  handleMenuClose: PropTypes.func.isRequired,
};

MobileNav.defaultProps = {
  mobileNavAnchorEl: null,
};

export default MobileNav;

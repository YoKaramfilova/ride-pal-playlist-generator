import { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Divider from '@material-ui/core/Divider';
import { FiltersContext } from 'providers/FiltersContext';
import { genresList } from 'common/filters.js';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
  },
}));

const FilterByGenre = () => {
  const classes = useStyles();
  const { genresFilter, setGenresFilter } = useContext(FiltersContext);

  const handleChange = (event) => {
    setGenresFilter({ ...genresFilter, [event.target.name]: event.target.checked });
  };

  return (
    <FormControl
      component="fieldset"
      className={classes.formControl}
    >
      <Typography variant='subtitle1'>
        Genres
      </Typography>
      <Divider />
      <FormGroup>
        {Object.values(genresList).map((genre) => (
          <FormControlLabel
            key={genre}
            control={(
              <Checkbox
                checked={genresFilter[genre]}
                onChange={handleChange}
                name={genre}
              />
            )}
            label={genre}
          />
        ))}
      </FormGroup>
    </FormControl>
  );
};

export default FilterByGenre;

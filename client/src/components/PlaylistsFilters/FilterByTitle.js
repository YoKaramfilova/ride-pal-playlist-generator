import { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';

import { FiltersContext } from 'providers/FiltersContext';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
  },
  fieldControl: {
    marginTop: theme.spacing(1),
  },
}));

const FilterByTitle = () => {
  const classes = useStyles();
  const { titleFilter, setTitleFilter } = useContext(FiltersContext);

  const handleInputValue = (event) => {
    setTitleFilter(event.target.value);
  };

  return (
    <FormControl
      component="fieldset"
      className={classes.formControl}
    >
      <Typography variant='subtitle1'>
        Title
      </Typography>
      <Divider />
      <Grid
        item
        className={classes.fieldControl}
      >
        <TextField
          variant="outlined"
          placeholder="Search by title..."
          value={titleFilter}
          onChange={handleInputValue}
        />
      </Grid>
    </FormControl>
  );
};

export default FilterByTitle;

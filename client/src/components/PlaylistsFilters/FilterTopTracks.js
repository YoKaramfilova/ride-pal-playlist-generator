import { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { FiltersContext } from 'providers/FiltersContext';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
  },
}));

const FilterTopPlaylists = () => {
  const classes = useStyles();
  const { topPlaylistsFilter, setTopPlaylistsFilter } = useContext(FiltersContext);

  const handleChange = () => {
    setTopPlaylistsFilter(!topPlaylistsFilter);
  };

  return (
    <FormControl
      component="fieldset"
      className={classes.formControl}
    >
      <Typography variant='subtitle1'>
        Rank
      </Typography>
      <Divider />
      <FormControlLabel
        control={<Switch checked={topPlaylistsFilter} onChange={handleChange} />}
        label="Only top playlists"
      />
    </FormControl>
  );
};

export default FilterTopPlaylists;

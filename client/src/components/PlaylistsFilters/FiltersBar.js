import { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import PropTypes from 'prop-types';

import { FiltersContext } from 'providers/FiltersContext';
import SortBy from 'components/PlaylistsFilters/SortBy';
import FilterByGenre from 'components/PlaylistsFilters/FilterByGenre';
import FilterByTitle from 'components/PlaylistsFilters/FilterByTitle';
import FilterTopTracks from 'components/PlaylistsFilters/FilterTopTracks';
import FilterByPlaytime from 'components/PlaylistsFilters/FilterByPlaytime';

const useStyles = makeStyles((theme) => ({
  drawerPaper: {
    overflowX: 'hidden',
  },
  drawerFooter: {
    margin: theme.spacing(1),
  }
}));

const FiltersBar = ({
  open, closeFiltersBar, setIsSearchTriggered, setPage, fromAdmin
}) => {
  const classes = useStyles();
  const { setIsFilterApplied, resetFiltersContext } = useContext(FiltersContext);

  const handleFiltersBar = () => {
    setIsSearchTriggered(true);
    // eslint-disable-next-line no-unused-expressions
    fromAdmin ? setPage(0) : setPage(1);
    setIsFilterApplied(true);
    closeFiltersBar();
  };

  const handleResetFiltersBar = () => {
    resetFiltersContext();
    setIsSearchTriggered(true);
    closeFiltersBar();
  };

  return (
    <Drawer
      variant="temporary"
      anchor="top"
      open={open}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <Grid
        container
        spacing={2}
        justify="center"
      >
        <Grid item>
          <FilterByTitle />
        </Grid>
        <Divider />
        <Grid item>
          <FilterByGenre />
        </Grid>
        <Divider />
        <Grid item>
          <FilterByPlaytime />
        </Grid>
        <Divider />
        <Grid item>
          <FilterTopTracks />
        </Grid>
        <Divider />
        <Grid item>
          <SortBy />
        </Grid>
        <Divider />
      </Grid>
      <Grid
        container
        spacing={2}
        justify="center"
        className={classes.drawerFooter}
      >
        <Grid item>
          <Button
            onClick={handleResetFiltersBar}
            color="primary"
            variant="outlined"
          >
            Clear filters
          </Button>
        </Grid>
        <Grid item>
          <Button
            onClick={handleFiltersBar}
            color="secondary"
            variant="contained"
          >
            Apply filters
          </Button>
        </Grid>
      </Grid>
    </Drawer>
  );
};

FiltersBar.propTypes = {
  closeFiltersBar: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  setPage: PropTypes.func.isRequired,
  setIsSearchTriggered: PropTypes.func.isRequired,
  fromAdmin: PropTypes.string,
};

FiltersBar.defaultProps = {
  fromAdmin: null,
};

export default FiltersBar;

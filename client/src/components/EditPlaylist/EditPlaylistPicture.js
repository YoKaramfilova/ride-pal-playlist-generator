import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import FormHelperText from '@material-ui/core/FormHelperText';
import Avatar from '@material-ui/core/Avatar';
import PropTypes from 'prop-types';

import unsplashService from 'services/unsplash';
import utils from 'common/utils';

const useStyles = makeStyles((theme) => ({
  pictureContainer: {
    '&:hover > $overlay': {
      display: 'block',
    },
    '&:hover > $picture': {
      filter: 'brightness(60%)',
    },
    position: 'relative',
    width: theme.spacing(30),
    height: theme.spacing(30),
  },
  picture: {
    position: 'absolute',
    top: '0%',
    left: '0%',
    width: theme.spacing(30),
    height: theme.spacing(30),
  },
  overlay: {
    display: 'none',
    position: 'absolute',
    width: '100%',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
}));

const EditPlaylistPicture = ({ form, setForm, setIsFormValid }) => {
  const classes = useStyles();
  const setRandomPicture = () => {
    unsplashService.getRandomRockImage()
      .then(res => {
        utils.setFormControlImageURL('picture', res.urls.regular, form, setForm, setIsFormValid);
      });
  };

  return (
    <Grid
      item
      className={classes.pictureContainer}
    >
      <Avatar
        variant="rounded"
        className={classes.picture}
        src={form.picture.preview}
        alt={form.picture.name}
      />
      <Grid
        item
        className={classes.overlay}
      >
        <Grid
          item
          container
          justify="center"
          spacing={2}
        >
          <Grid item>
            {/* eslint-disable jsx-a11y/label-has-associated-control */}
            <input
              id="image-file-input"
              name={form.picture.name}
              type="file"
              accept="image/png, image/jpeg"
              onChange={(e) => {
                utils.handleInputChange(e, form, setForm, setIsFormValid);
              }}
              style={{ display: 'none', }}
            />
            <label htmlFor="image-file-input">
              <Button
                variant="contained"
                color="secondary"
                component="span"
              >
                Upload cover
              </Button>
            </label>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              onClick={setRandomPicture}
            >
              Or set random
            </Button>
          </Grid>
        </Grid>
      </Grid>
      { (form.picture.touched
        && !form.picture.valid)
        && (
          <Grid item>
            <FormHelperText error>
              Picture should be in .png, .jpg or .jpeg.
            </FormHelperText>
          </Grid>
        )}
    </Grid>
  );
};

EditPlaylistPicture.propTypes = {
  form: PropTypes.shape({
    title: PropTypes.shape({
      name: PropTypes.string,
      placeholder: PropTypes.string,
      value: PropTypes.string,
      validation: PropTypes.shape({
        required: PropTypes.bool,
        minLength: PropTypes.number,
        maxLength: PropTypes.number,
      }),
      valid: PropTypes.bool,
      touched: PropTypes.bool,
    }),
    picture: PropTypes.shape({
      name: PropTypes.string,
      value: PropTypes.string,
      files: PropTypes.objectOf(PropTypes.any),
      imageURL: PropTypes.string,
      preview: PropTypes.string,
      validation: PropTypes.shape({
        validFileTypes: PropTypes.arrayOf(PropTypes.string),
      }),
      valid: PropTypes.bool,
      touched: PropTypes.bool,
    }),
  }),
  setForm: PropTypes.func,
  setIsFormValid: PropTypes.func,
};

EditPlaylistPicture.defaultProps = {
  form: {
    title: null,
    picture: null,
  },
  setForm: null,
  setIsFormValid: null,
};

export default EditPlaylistPicture;

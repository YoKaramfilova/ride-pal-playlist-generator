import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Grid from '@material-ui/core/Grid';
import _ from 'lodash';
import PropTypes from 'prop-types';

import EditPlaylistText from 'components/EditPlaylist/EditPlaylistText';
import EditPlaylistPicture from 'components/EditPlaylist/EditPlaylistPicture';
import ActionAlert from 'components/ActionAlert/ActionAlert';
import Loading from 'components/Loading/Loading';
import formControls from 'common/form-controls';
import playlistsService from 'services/playlists';

const EditPlaylist = ({
  isOpen, handleClose, playlist, setPlaylist
}) => {
  const history = useHistory();

  const [isFormValid, setIsFormValid] = useState(false);
  const [form, setForm] = useState(_.cloneDeep(formControls.EDIT_PLAYLIST_FORM_CONTROLS));

  const [loading, setLoading] = useState(false);
  const [notification, setNotification] = useState({ isOpen: false, type: '', message: '' });
  const handleNotificationClose = () => setNotification({ isOpen: false, type: '', message: '' });

  useEffect(() => {
    if (Object.keys(playlist).length !== 0) {
      Object
        .entries(playlist)
        .forEach(prop => {
          const [key, value] = prop;

          if (form[key]) {
            const updatedControl = form[key];

            updatedControl.value = value;
            updatedControl.valid = true;

            if (key === 'picture' && value) {
              updatedControl.preview = value.includes('unsplash')
                ? value
                : `/images/${value}`;
            }
          }
        });

      setForm({ ...form });
      setIsFormValid(true);
    }
  }, [playlist]);

  const formSubmitHandler = (e) => {
    e.preventDefault();

    const formData = new FormData();

    if (form.title.value && form.title.value !== playlist.title) {
      formData.append('title', form.title.value);
    }

    if (form.picture.files) {
      formData.append('image', form.picture.files[0], form.picture.files[0].filename);
    } else if (form.picture.imageURL) {
      formData.append('imageURL', form.picture.imageURL);
    }

    setLoading(true);

    playlistsService.updatePlaylistDetails(playlist.id, formData)
      .then((res) => {
        if (res.message) {
          setNotification({
            isOpen: true,
            type: 'error',
            message: res.message,
          });
        } else if (res.errors) {
          const message = Object
            .entries(res.errors)
            .map(error => `${error[0]}: ${error[1]} `)
            .join('\n');

          setNotification({
            isOpen: true,
            type: 'error',
            message,
          });
        } else {
          setNotification({
            isOpen: true,
            type: 'success',
            message: 'Successfully updated playlist.'
          });

          if (form.title.value) {
            setPlaylist((prev) => ({ ...prev, title: res.title }));
          }

          if (form.picture.files || form.picture.imageURL) {
            setPlaylist((prev) => ({ ...prev, picture: res.picture }));
          }
        }

        setForm(_.cloneDeep(formControls.EDIT_PLAYLIST_FORM_CONTROLS));
      })
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
  };

  return (
    <>
      <Dialog open={isOpen} onClose={handleClose}>
        <DialogTitle>Edit playlist details</DialogTitle>
        <DialogContent>
          <Grid
            item
            container
            spacing={4}
            justify="center"
            alignItems="center"
            component="form"
          >

            <EditPlaylistPicture
              form={form}
              setForm={setForm}
              isFormValid={isFormValid}
              setIsFormValid={setIsFormValid}
            />
            <EditPlaylistText
              form={form}
              setForm={setForm}
              isFormValid={isFormValid}
              setIsFormValid={setIsFormValid}
            />

          </Grid>

          {loading && <Loading />}
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            disabled={!isFormValid || !Object.values(form).some(
              (control) => control.touched
            )}
            color="secondary"
            onClick={(e) => {
              formSubmitHandler(e);
              handleClose();
            }}
          >
            Save
          </Button>
          <Button
            onClick={handleClose}
            color="primary"
          >
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
      <ActionAlert
        notification={notification}
        handleClose={handleNotificationClose}
      />
    </>
  );
};

EditPlaylist.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  playlist: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    picture: PropTypes.string,
  }),
  setPlaylist: PropTypes.func.isRequired,
};

EditPlaylist.defaultProps = {
  playlist: {
    id: null,
    title: null,
    picture: null,
  },
};

export default EditPlaylist;

import TextField from '@material-ui/core/TextField';

import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import utils from 'common/utils';

const EditPlaylistText = ({
  form, setForm, setIsFormValid
}) => (
  <Grid item>
    <TextField
      variant="outlined"
      name={form.title.name}
      placeholder={form.title.placeholder}
      value={form.title.value}
      error={form.title.touched && !form.title.valid}
      required
      fullWidth
      label={form.title.touched
                  && !form.title.valid
        ? 'Error'
        : ''}
      helperText={form.title.touched
                  && !form.title.valid
        ? 'Title should be between 1 and 255 characters.'
        : ''}
      onChange={(e) => {
        utils.handleInputChange(e, form, setForm, setIsFormValid);
      }}
    />
  </Grid>
);

EditPlaylistText.propTypes = {
  form: PropTypes.shape({
    title: PropTypes.shape({
      name: PropTypes.string,
      placeholder: PropTypes.string,
      value: PropTypes.string,
      validation: PropTypes.shape({
        required: PropTypes.bool,
        minLength: PropTypes.number,
        maxLength: PropTypes.number,
      }),
      valid: PropTypes.bool,
      touched: PropTypes.bool,
    }),
    picture: PropTypes.shape({
      name: PropTypes.string,
      value: PropTypes.string,
      files: PropTypes.objectOf(PropTypes.any),
      imageURL: PropTypes.string,
      preview: PropTypes.string,
      validation: PropTypes.shape({
        validFileTypes: PropTypes.arrayOf(PropTypes.string),
      }),
      valid: PropTypes.bool,
      touched: PropTypes.bool,
    }),
  }),
  setForm: PropTypes.func,
  setIsFormValid: PropTypes.func,
};

EditPlaylistText.defaultProps = {
  form: {
    title: null,
    picture: null,
  },
  setForm: null,
  setIsFormValid: null,
};

export default EditPlaylistText;

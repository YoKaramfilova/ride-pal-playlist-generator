import axiosData, { ENDPOINTS } from 'services/serviceBase';

const register = (username, email, password) => axiosData.request({
  method: 'post',
  url: ENDPOINTS.USERS,
  data: { username, email, password },
});

const getUser = (id) => axiosData.request({
  method: 'get',
  url: `${ENDPOINTS.USERS}/${id}`,
});

const updateUserDetails = (id, formData) => axiosData.request({
  method: 'put',
  url: `${ENDPOINTS.USERS}/${id}`,
  data: formData,
});

const changePassword = (id, newPassword, oldPassword) => axiosData.request({
  method: 'patch',
  url: `${ENDPOINTS.USERS}/${id}`,
  data: { newPassword, oldPassword },
});

export default {
  register, getUser, updateUserDetails, changePassword
};

import axiosData, { ENDPOINTS } from 'services/serviceBase';

const getAllArtists = () => axiosData.request({
  method: 'get',
  url: ENDPOINTS.ARTISTS,
});

export default { getAllArtists };

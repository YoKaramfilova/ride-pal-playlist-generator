import axios from 'axios';

export const API_VERSION = '/v1';

export const ENDPOINTS = {
  PLAYLISTS: `${API_VERSION}/playlists`,
  USERS: `${API_VERSION}/users`,
  ADMIN: `${API_VERSION}/admin`,
  LOGIN: `${API_VERSION}/login`,
  LOGOUT: `${API_VERSION}/logout`,
  GENRES: `${API_VERSION}/genres`,
  TRACKS: `${API_VERSION}/tracks`,
  ARTISTS: `${API_VERSION}/artists`,
};

const axiosData = axios.create({
  validateStatus: (status) => status >= 200 && status < 500,
});

axiosData.interceptors.request.use(request => {
  request.headers.authorization = localStorage.getItem('token')
    ? `Bearer ${localStorage.getItem('token')}`
    : '';

  return request;
});

axiosData.interceptors.response.use((response) => response.data);

export default axiosData;

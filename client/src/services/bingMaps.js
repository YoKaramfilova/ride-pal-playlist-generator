const BING_KEY = 'AmqtWU4SK30MQyYDt3xKRrnuQ7HNyn_vxGe2q0ZJORuNVvf1KkwdFZdcRP51Zp8x';
const BING_API_LOCATIONS = 'https://dev.virtualearth.net/REST/v1/Locations';
const BING_API_DISTANCE = 'https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix';

const findDistance = (startPoint, endPoint) => (
  fetch(`${BING_API_DISTANCE}?origins=${startPoint.join(',')}&destinations=${endPoint.join(',')}&travelMode=driving&key=${BING_KEY}&timeUnit=second`, {
    method: 'GET',
    redirect: 'manual',
  }).then(res => res.json()));

const findLocationByAddress = (address) => (
  fetch(`${BING_API_LOCATIONS}?query=${address}&key=${BING_KEY}`, {
    method: 'GET',
    redirect: 'manual',
  }).then(res => res.json()));

export default {
  BING_KEY,
  findDistance,
  findLocationByAddress,
};

import axios from 'axios';

const UNSPLASH_API_KEY = 'ssY9rkYUKZuvXI4CUxeoN3rlm6Ljld4i45oetBubyQs';

const unsplashData = axios.create();
unsplashData.interceptors.response.use((response) => response.data);

const getRandomRockImage = () => unsplashData({
  method: 'get',
  url: `https://api.unsplash.com/photos/random?client_id=${UNSPLASH_API_KEY}&query=rock_music`,
});

export default {
  getRandomRockImage,
};

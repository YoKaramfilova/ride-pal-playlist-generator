import {
  useState, useEffect, useContext, useRef, useCallback
} from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';
import Skeleton from '@material-ui/lab/Skeleton';
import { DateTime } from 'luxon';

import Loading from 'components/Loading/Loading';
import ActionAlert from 'components/ActionAlert/ActionAlert';
import PlaylistSimple from 'components/Playlist/PlaylistSimple';
import { AuthContext } from 'providers/АuthContext';
import playlistsService from 'services/playlists';
import usersService from 'services/user';
import UserSettingsMenu from 'pages/UserProfile/UserSettingsMenu';
import UserDetailsForm from 'pages/UserProfile/UserDetailsForm';
import UserPasswordForm from 'pages/UserProfile/UserPasswordForm';

const useStyles = makeStyles((theme) => ({
  avatarContainer: {
    '&:hover > $overlay': {
      display: 'block',
    },
    '&:hover > $avatar': {
      filter: 'brightness(60%)',
    },
    position: 'relative',
  },
  avatar: {
    width: theme.spacing(20),
    height: theme.spacing(20),
  },
  overlay: {
    color: '#FAFAFA',
    display: 'none',
    position: 'absolute',
    top: '50%',
    left: '50%',
    fontSize: theme.spacing(6),
    transform: 'translate(-50%, -50%)',
  },
}));

const UserProfile = () => {
  const classes = useStyles();
  const history = useHistory();
  const observer = useRef();
  const auth = useContext(AuthContext);
  const { state: userId } = useLocation();

  const [user, setUser] = useState({});

  const [settingsMenuAnchorEl, setSettingsMenuAnchorEl] = useState(null);
  const isSettingsMenuOpen = Boolean(settingsMenuAnchorEl);
  const [isDetailsFormOpen, setDetailsFormOpen] = useState(false);
  const handleDetailsFormClose = () => setDetailsFormOpen(false);
  const handleDetailsFormOpen = () => setDetailsFormOpen(true);
  const [isPasswordFormOpen, setPasswordFormOpen] = useState(false);
  const handlePasswordFormClose = () => setPasswordFormOpen(false);
  const handlePasswordFormOpen = () => setPasswordFormOpen(true);

  const [playlists, setPlaylists] = useState([]);
  const [totalPlaylists, setTotalPlaylists] = useState(0);
  const [page, setPage] = useState(1);
  const [itemsPerPage] = useState(8);
  const [hasMore, setHasMore] = useState(false);

  const [loading, setLoading] = useState(false);
  const [notification, setNotification] = useState({ isOpen: false, type: '', message: '' });
  const handleNotificationClose = () => setNotification({ isOpen: false, type: '', message: '' });

  const handleSettingsMenuOpen = (event) => {
    event.preventDefault();
    setSettingsMenuAnchorEl(event.currentTarget);
  };

  const handleSettingsMenuClose = () => {
    setSettingsMenuAnchorEl(null);
  };

  if (playlists.some(playlist => playlist.user_id !== userId)) {
    setPlaylists([]);
    setPage(1);
    setHasMore(false);
  }

  useEffect(() => {
    setLoading(true);

    usersService.getUser(userId)
      .then((result) => {
        if (result.message) {
          setNotification({
            isOpen: true,
            type: 'error',
            message: result.message,
          });
        } else {
          setUser(result);
        }
      })
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
  }, [userId]);

  useEffect(() => {
    setLoading(true);

    playlistsService.getAllPlaylists({
      userId, sortBy: 'created_on.desc', limit: itemsPerPage, offset: (page - 1) * itemsPerPage
    })
      .then((result) => {
        setPlaylists(prev => [...prev, ...result.data]);
        if (result.total) setTotalPlaylists(result.total);
        setHasMore(result.data.length > 0);
      })
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
  }, [page, userId]);

  const lastPlaylistElementRef = useCallback(element => {
    if (loading) return;
    if (observer.current) observer.current.disconnect();
    observer.current = new IntersectionObserver(entries => {
      if (entries[0].isIntersecting && hasMore) {
        setPage(prev => prev + 1);
      }
    });
    if (element) observer.current.observe(element);
  }, [hasMore, loading]);

  return (
    <Container
      disableGutters
      maxWidth="lg"
    >
      <Grid
        container
        spacing={4}
        className='resetContainerWithSpacing'
      >
        <Grid
          item
          container
          alignItems="flex-start"
          justify="space-between"
        >
          <Grid item>
            <Grid
              container
              alignItems="flex-end"
              spacing={4}
            >
              <Grid
                item
                className={userId === auth.user.id
                  ? classes.avatarContainer
                  : ''}
              >
                {loading && !user.avatar
                  ? (
                    <Skeleton variant="circle" width={150} height={150} />) : (
                      <Avatar
                        className={classes.avatar}
                        alt={user.username}
                        src={`/images/${user.avatar}`}
                      />
                  )}

                {user.id === auth.user.id && (
                  <AddPhotoAlternateIcon
                    className={classes.overlay}
                    onClick={handleDetailsFormOpen}
                  />
                )}
              </Grid>
              <Grid item>
                {!loading && !user.username ? (
                  <Typography
                    variant="subtitle1"
                    color="textSecondary"
                  >
                    User deleted
                  </Typography>
                )
                  : (
                    <>
                      <Typography
                        variant="h5"
                        color="textPrimary"
                      >
                        {loading && !user.username
                          ? (
                            <Skeleton height={40} width={180} />) : (user.username)}
                      </Typography>
                      <Typography
                        variant="subtitle1"
                        color="textSecondary"
                      >
                        {loading && !user.created_on
                          ? (
                            <Skeleton height={30} width={190} />) : (`User since: ${DateTime.fromISO(user.created_on).toFormat('dd LLL yyyy')}`)}
                      </Typography>
                    </>
                  )}
                <Typography
                  variant="subtitle1"
                  color="textSecondary"
                >
                  {loading && !totalPlaylists
                    ? (
                      <Skeleton height={30} width={150} />) : (`Total playlists: ${totalPlaylists}`)}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            {user.id === auth.user.id && (
              <>
                <IconButton
                  color="primary"
                  onClick={handleSettingsMenuOpen}
                >
                  <MoreHorizIcon fontSize="large" />
                </IconButton>
                <UserSettingsMenu
                  handleClose={handleSettingsMenuClose}
                  anchorEl={settingsMenuAnchorEl}
                  isOpen={isSettingsMenuOpen}
                  handleDetailsFormOpen={handleDetailsFormOpen}
                  handlePasswordFormOpen={handlePasswordFormOpen}
                />
              </>
            )}
          </Grid>
        </Grid>
        <Divider style={{ width: '100%' }} />
        <Grid
          item
          container
          spacing={2}
        >
          <Grid item container>
            <Typography
              variant="h5"
              color="textPrimary"
            >
              {!loading && playlists.length === 0
                ? 'No playlists yet'
                : 'Playlists'}
            </Typography>
          </Grid>
          {playlists.map((playlist, index) => {
            if (playlists.length === index + 1) {
              return (
                <Grid
                  item
                  xs={12}
                  sm={6}
                  md={3}
                  ref={lastPlaylistElementRef}
                  key={playlist.id}
                >
                  <PlaylistSimple
                    {...playlist}
                  />
                </Grid>
              );
            }

            return (
              <Grid
                item
                xs={12}
                sm={6}
                md={3}
                key={playlist.id}
              >
                <PlaylistSimple
                  {...playlist}
                />
              </Grid>
            );
          })}
          {loading && <Loading />}
        </Grid>
        <UserDetailsForm
          isOpen={isDetailsFormOpen}
          handleClose={handleDetailsFormClose}
          user={user}
          setUser={setUser}
        />
        <UserPasswordForm
          isOpen={isPasswordFormOpen}
          handleClose={handlePasswordFormClose}
          user={user}
          setUser={setUser}
        />
        <ActionAlert
          notification={notification}
          handleClose={handleNotificationClose}
        />
      </Grid>
    </Container>
  );
};

export default UserProfile;

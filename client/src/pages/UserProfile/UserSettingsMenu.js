import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';

const UserSettingsMenu = ({
  isOpen,
  handleClose,
  anchorEl,
  handleDetailsFormOpen,
  handlePasswordFormOpen,
}) => (
  <Menu
    anchorEl={anchorEl}
    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
    keepMounted
    transformOrigin={{ vertical: 'top', horizontal: 'right' }}
    open={isOpen}
    onClose={handleClose}
  >
    <Grid>
      <MenuItem
        onClick={() => {
          handleClose();
          handleDetailsFormOpen();
        }}
      >
        Edit profile
      </MenuItem>
      <MenuItem
        onClick={() => {
          handleClose();
          handlePasswordFormOpen();
        }}
      >
        Change password
      </MenuItem>
    </Grid>
  </Menu>
);

UserSettingsMenu.propTypes = {
  anchorEl: PropTypes.instanceOf(Element),
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  handleDetailsFormOpen: PropTypes.func.isRequired,
  handlePasswordFormOpen: PropTypes.func.isRequired,
};

UserSettingsMenu.defaultProps = {
  anchorEl: null,
};

export default UserSettingsMenu;

import { useContext, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import {
  Container, Grid, TextField, Button,
  Avatar, Typography, Link
} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import _ from 'lodash';
import decode from 'jwt-decode';

import authService from 'services/auth';
import formControls from 'common/form-controls';
import utils from 'common/utils';
import ActionAlert from 'components/ActionAlert/ActionAlert';
import Loading from 'components/Loading/Loading';
import { AuthContext } from 'providers/АuthContext';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  linkItem: {
    textAlign: 'end'
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Login = () => {
  const classes = useStyles();
  const history = useHistory();
  const auth = useContext(AuthContext);
  const [notification, setNotification] = useState({ isOpen: false, type: '', message: '' });
  const handleNotificationClose = () => setNotification({ isOpen: false, type: '', message: '' });

  const [loading, setLoading] = useState(false);
  const [isFormValid, setIsFormValid] = useState(false);
  const [form, setForm] = useState(_.cloneDeep(formControls.LOGIN_FORM_CONTROLS));

  useEffect(() => {
    if (localStorage.exp) {
      setNotification({
        isOpen: true,
        type: 'warning',
        message: 'Your token has expired! Please login again!',
      });
    }
  }, []);

  const formSubmitHandler = (e) => {
    e.preventDefault();

    setLoading(true);

    authService.login(form.username.value, form.password.value)
      .then((res) => {
        if (res.message) {
          setNotification({
            isOpen: true,
            type: 'error',
            message: res.message,
          });
        } else {
          localStorage.clear();
          localStorage.setItem('token', res.token);
          const user = decode(res.token);
          auth.setUser(user);
          history.push('/home');
        }
      })
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
  };

  return (
    <Container disableGutters maxWidth="xs">
      <Grid
        container
        spacing={2}
        component='form'
        className='resetContainerWithSpacing'
      >
        <Grid item container xs={12} direction='column' alignItems='center'>
          <Grid item>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
          </Grid>
          <Grid item>
            <Typography component="h1" variant="h5">
              Login
            </Typography>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <TextField
            name={form.username.name}
            placeholder={form.username.placeholder}
            value={form.username.value}
            variant="outlined"
            error={form.username.touched && !form.username.valid}
            required
            fullWidth
            label={form.username.touched
              && !form.username.valid
              ? 'Error'
              : ''}
            helperText={form.username.touched
              && !form.username.valid
              ? 'Username is required.'
              : ''}
            onChange={(e) => {
              utils.handleInputChange(e, form, setForm, setIsFormValid);
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name={form.password.name}
            placeholder={form.password.placeholder}
            value={form.password.value}
            type="password"
            variant="outlined"
            error={form.password.touched && !form.password.valid}
            required
            fullWidth
            label={form.password.touched
              && !form.password.valid
              ? 'Error'
              : ''}
            helperText={form.password.touched
              && !form.password.valid
              ? 'Password is required.'
              : ''}
            onChange={(e) => {
              utils.handleInputChange(e, form, setForm, setIsFormValid);
            }}
          />
        </Grid>
        {loading && <Loading />}
        <Grid item xs={12}>
          <Button
            variant="contained"
            disabled={!isFormValid || !Object.values(form).some(
              (control) => control.touched
            )}
            color="secondary"
            fullWidth
            className={classes.submit}
            onClick={formSubmitHandler}
          >
            Login
          </Button>
        </Grid>
        <Grid item xs={12} className={classes.linkItem}>
          <Link href="/register" variant="body2">
            Don`t have an account? Sign up
          </Link>
        </Grid>
      </Grid>
      <ActionAlert
        notification={notification}
        handleClose={handleNotificationClose}
      />
    </Container>
  );
};

export default Login;

import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import _ from 'lodash';
import { Duration } from 'luxon';

import TripStartEndPoint from 'pages/CreatePlaylist/TripStartEndPoint';
import TripDuration from 'pages/CreatePlaylist/TripDuration';
import Genres from 'pages/CreatePlaylist/Genres';
import Preferences from 'pages/CreatePlaylist/Preferences';
import Personalization from 'pages/CreatePlaylist/Personalization';
import Confirmation from 'pages/CreatePlaylist/Confirmation';
import Review from 'pages/CreatePlaylist/Review';

import ActionAlert from 'components/ActionAlert/ActionAlert';
import Loading from 'components/Loading/Loading';

import bingMapsService from 'services/bingMaps';
import formControls from 'common/form-controls';
import utils from 'common/utils';
import steps from 'common/playlist-creation-steps';

import playlistService from 'services/playlists';

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(3),
    height: '100%'
  },
  stepperContainer: {
    height: 'calc(100% + 16px)',
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  stepperHeader: {
    flexGrow: 0,
  },
  stepperFooter: {
    flexGrow: 0,
  },
  stepperContent: {
    flexGrow: 1,
  },
  stepper: {
    padding: theme.spacing(3, 0, 5),
  },
}));

const getStepContent = (
  step,
  startAddress,
  endAddress,
  setStartAddress,
  setEndAddress,
  setStartPoint,
  setEndPoint,
  setIsStartPointValid,
  setIsEndPointValid,
  form,
  setForm,
  setIsFormValid,
  error,
  durationTrip,
) => {
  switch (step) {
    case steps.START_AND_END_POINTS.step:
      return (
        <TripStartEndPoint
          startAddress={startAddress}
          endAddress={endAddress}
          setStartAddress={setStartAddress}
          setEndAddress={setEndAddress}
          setStartPoint={setStartPoint}
          setEndPoint={setEndPoint}
          setIsStartPointValid={setIsStartPointValid}
          setIsEndPointValid={setIsEndPointValid}
        />
      );
    case steps.DURATION.step:
      return (
        <TripDuration
          form={form}
          setForm={setForm}
          setIsFormValid={setIsFormValid}
          durationTrip={durationTrip}
        />
      );
    case steps.GENRES.step:
      return (
        <Genres
          form={form}
          setForm={setForm}
          setIsFormValid={setIsFormValid}
        />
      );
    case steps.PREFERENCES.step:
      return (
        <Preferences
          form={form}
          setForm={setForm}
          setIsFormValid={setIsFormValid}
        />
      );
    case steps.PERSONALIZATION.step:
      return (
        <Personalization
          form={form}
          setForm={setForm}
          setIsFormValid={setIsFormValid}
        />
      );
    case steps.CONFIRMATION.step:
      return (
        <Confirmation
          error={error}
        />
      );
    default:
      return <Typography>Unknown step</Typography>;
  }
};

const CreatePlaylistStepper = () => {
  const history = useHistory();
  const classes = useStyles();

  const [activeStep, setActiveStep] = useState(0);

  const [startAddress, setStartAddress] = useState('');
  const [endAddress, setEndAddress] = useState('');
  const [startPoint, setStartPoint] = useState([]);
  const [endPoint, setEndPoint] = useState([]);
  const [isStartPointValid, setIsStartPointValid] = useState(false);
  const [isEndPointValid, setIsEndPointValid] = useState(false);

  const [form, setForm] = useState(_.cloneDeep(formControls.PLAYLIST_FORM_CONTROLS));
  const [isFormValid, setIsFormValid] = useState(false);
  const [playlist, setPlaylist] = useState({});

  const [notification, setNotification] = useState({ isOpen: false, type: '', message: '' });
  const handleNotificationClose = () => setNotification({ isOpen: false, type: '', message: '' });
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [durationTrip, setDurationTrip] = useState();

  const handleNext = () => {
    if (activeStep === steps.START_AND_END_POINTS.step) {
      if (isStartPointValid && isEndPointValid) {
        setLoading(true);

        bingMapsService.findDistance(startPoint, endPoint)
          .then(result => {
            if (result.resourceSets[0].resources[0].results[0].travelDuration < 0) {
              setNotification({
                isOpen: true,
                type: 'error',
                message: 'We cannot find car directions for this journey. Check your waypoints and route options and try again.',
              });
            }
            setDurationTrip(result.resourceSets[0].resources[0].results[0].travelDuration);
            const duration = Duration
              .fromMillis(result.resourceSets[0].resources[0].results[0].travelDuration * 1000)
              .toFormat('hh:mm:ss');
            utils.setFormControlValue('duration', duration, form, setForm, setIsFormValid);
          })
          .catch(() => history.push('/500'))
          .finally(() => setLoading(false));

        setActiveStep(activeStep + 1);
      } else {
        setNotification({
          isOpen: true,
          type: 'error',
          message: 'Enter valid start and end points.',
        });
      }
    } else if (activeStep === steps.DURATION.step) {
      if (form.duration.valid) {
        setActiveStep(activeStep + 1);
      } else {
        setNotification({
          isOpen: true,
          type: 'error',
          message: 'Enter valid trip duration.',
        });
      }
    } else if (activeStep === steps.GENRES.step) {
      if (form.genres.valid) {
        setActiveStep(activeStep + 1);
      } else {
        setNotification({
          isOpen: true,
          type: 'error',
          message: 'Genre mix should amount to 100%.',
        });
      }
    } else if (activeStep === steps.PERSONALIZATION.step) {
      if (form.title.valid && form.picture.valid) {
        setActiveStep(activeStep + 1);
      } else {
        setNotification({
          isOpen: true,
          type: 'error',
          message: 'Enter valid name and cover for the playlist.',
        });
      }
    } else if (activeStep === steps.CONFIRMATION.step) {
      if (isFormValid) {
        setLoading(true);

        const formData = new FormData();
        formData.append('title', form.title.value);
        formData.append('duration', Duration.fromISOTime(form.duration.value).valueOf() / 1000);
        formData.append('genres', JSON.stringify(form.genres.value));
        if (form.topTracks.value) {
          formData.append('topTracks', form.topTracks.value);
        }

        if (form.repeatArtists.value) {
          formData.append('repeatArtists', form.repeatArtists.value);
        }

        if (form.picture.files) {
          formData.append('image', form.picture.files[0], form.picture.files[0].filename);
        } else {
          formData.append('imageURL', form.picture.imageURL);
        }

        playlistService.createPlaylist(formData)
          .then(result => {
            if (result.message) {
              setError(result.message);
            } else if (result.errors) {
              const message = Object
                .entries(result.errors)
                .map(err => `${err[0]}: ${err[1]} `)
                .join('\n');

              setError(message);
            } else {
              setPlaylist(result);
              setActiveStep(activeStep + 1);
            }
          })
          .catch(() => history.push('/500'))
          .finally(() => setLoading(false));
      } else {
        setNotification({
          isOpen: true,
          type: 'error',
          message: 'Go back and adjust your playlist preferences.',
        });
      }
    } else {
      setActiveStep(activeStep + 1);
    }
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
    setError('');
  };

  return (
    <Paper className={classes.paper}>
      <Grid
        container
        direction='column'
        className={classes.stepperContainer}
        wrap='nowrap'
        spacing={2}
      >
        <Grid item className={classes.stepperHeader}>
          <Typography
            variant="inherit"
            component="h1"
            align="center"
            color="primary"
          >
            CREATE PLAYLIST
          </Typography>
          <Stepper
            activeStep={activeStep}
            className={classes.stepper}
          >
            {Object.values(steps).map(({ label }) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
        </Grid>
        <Grid
          item
          className={classes.stepperContent}
        >
          {activeStep > steps.CONFIRMATION.step ? (
            <Review playlist={playlist} />
          ) : (
            <>
              {loading && <Loading />}
              {!loading
                  && getStepContent(
                    activeStep,
                    startAddress,
                    endAddress,
                    setStartAddress,
                    setEndAddress,
                    setStartPoint,
                    setEndPoint,
                    setIsStartPointValid,
                    setIsEndPointValid,
                    form,
                    setForm,
                    setIsFormValid,
                    error,
                    durationTrip,
                  )}
            </>
          )}
        </Grid>
        {activeStep <= steps.CONFIRMATION.step && (
          <Grid item className={classes.stepperFooter}>
            <Grid
              container
              justify="flex-end"
            >
              {activeStep !== steps.START_AND_END_POINTS.step && (
                <Button
                  onClick={handleBack}
                >
                  Back
                </Button>
              )}
              <Button
                variant="contained"
                color="secondary"
                onClick={handleNext}
              >
                {activeStep === steps.CONFIRMATION.step
                  ? 'Generate playlist'
                  : 'Next'}
              </Button>
            </Grid>
          </Grid>
        )}
      </Grid>
      <ActionAlert
        notification={notification}
        handleClose={handleNotificationClose}
      />
    </Paper>
  );
};

export default CreatePlaylistStepper;

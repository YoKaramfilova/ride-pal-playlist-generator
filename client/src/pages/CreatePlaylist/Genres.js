import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import PropTypes from 'prop-types';

import Loading from 'components/Loading/Loading';
import utils from 'common/utils';
import GenreSimple from 'components/Genre/GenreSimple';
import genresService from 'services/genres';

const Genres = ({ form, setForm, setIsFormValid }) => {
  const history = useHistory();

  const [loading, setLoading] = useState(false);
  const [genres, setGenres] = useState([]);

  useEffect(() => {
    setLoading(true);

    genresService.getAllGenres()
      .then((result) => setGenres(result))
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
  }, []);

  return (
    <Grid
      container
      spacing={6}
      direction='column'
      justify='center'
      wrap='nowrap'
    >
      <Grid item>
        <Typography
          variant="h6"
          gutterBottom
          align="center"
        >
          Adjust genres mix
        </Typography>
      </Grid>
      <Grid
        item
        container
        justify="center"
        alignItems="center"
        spacing={3}
      >
        {loading && <Loading />}
        {genres.map((genre) => (
          <Grid
            item
            container
            xs={12}
            sm={6}
            md={4}
            justify="center"
            alignContent="center"
            direction="column"
            spacing={6}
            key={genre.id}
          >
            <GenreSimple name={genre.name} picture={genre.picture} />
            <Grid item>
              <Slider
                value={form.genres.value[genre.id] ?? 0}
                min={0}
                max={100}
                step={5}
                valueLabelFormat={(value) => `${value}%`}
                valueLabelDisplay="on"
                onChange={(e, value) => {
                  utils.setFormControlValue('genres',
                    { ...form.genres.value, [genre.id]: value },
                    form,
                    setForm,
                    setIsFormValid);
                }}
                marks={[
                  {
                    value: 0,
                    label: '0%',
                  },
                  {
                    value: 100,
                    label: '100%',
                  },
                ]}
                color="secondary"
              />
            </Grid>
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
};

Genres.propTypes = {
  form: PropTypes.shape({
    genres: PropTypes.shape({
      value: PropTypes.objectOf(PropTypes.any).isRequired,
      validation: PropTypes.shape({
        valuesTotal: PropTypes.number.isRequired,
      }).isRequired,
      valid: PropTypes.bool.isRequired,
      touched: PropTypes.bool.isRequired,
    }).isRequired,
  }).isRequired,
  setForm: PropTypes.func.isRequired,
  setIsFormValid: PropTypes.func.isRequired,
};

export default Genres;

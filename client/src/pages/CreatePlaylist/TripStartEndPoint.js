/* eslint-disable react/no-array-index-key */
import { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PropTypes from 'prop-types';
import bingMapsService from 'services/bingMaps';

// eslint-disable-next-line import/no-unresolved
import BingMapsReact from 'bingmaps-react';

const useStyles = makeStyles(() => ({
  container: {
    height: '100%',
  },
  mapContainer: {
    minHeight: 300,
    height: '100%',
  },
}));

const TripStartEndPoint = ({
  startAddress,
  endAddress,
  setStartAddress,
  setEndAddress,
  setStartPoint,
  setEndPoint,
  setIsStartPointValid,
  setIsEndPointValid
}) => {
  const classes = useStyles();
  const [startSearch, setStartSearch] = useState([]);
  const [endSearch, setEndSearch] = useState([]);
  const [mapCenter, setMapCenter] = useState([42.698334, 23.319941]);
  const [pins, setPins] = useState([]);

  useEffect(() => {
    bingMapsService.findLocationByAddress(startAddress)
      .then(result => {
        const newPins = [...pins];
        newPins[0] = {
          center: {
            latitude: result.resourceSets[0].resources[0].point.coordinates[0],
            longitude: result.resourceSets[0].resources[0].point.coordinates[1]
          },
          options: { title: result.resourceSets[0].resources[0].name, text: 'Start' }
        };
        setPins(newPins);
        setStartSearch(result.resourceSets[0].resources
          .map(location => location.address.formattedAddress));
        setStartPoint(result.resourceSets[0].resources[0].point.coordinates);
        setMapCenter(result.resourceSets[0].resources[0].point.coordinates);
        setIsStartPointValid(true);
      })

      .catch(() => setIsStartPointValid(false));
  }, [startAddress]);

  useEffect(() => {
    bingMapsService.findLocationByAddress(endAddress)
      .then(result => {
        const newPins = [...pins];
        newPins[1] = {
          center: {
            latitude: result.resourceSets[0].resources[0].point.coordinates[0],
            longitude: result.resourceSets[0].resources[0].point.coordinates[1]
          },
          options: { title: result.resourceSets[0].resources[0].name, text: 'End' }
        };
        setPins(newPins);
        setEndSearch(result.resourceSets[0].resources
          .map(location => location.address.formattedAddress));
        setEndPoint(result.resourceSets[0].resources[0].point.coordinates);
        setMapCenter(result.resourceSets[0].resources[0].point.coordinates);
        setIsEndPointValid(true);
      })
      .catch(() => setIsEndPointValid(false));
  }, [endAddress]);

  return (
    <Grid
      container
      spacing={2}
      direction='column'
      justify='center'
      wrap='nowrap'
      className={classes.container}
    >
      <Grid item>
        <Typography
          variant="h6"
          gutterBottom
          align="center"
        >
          Set start and end points for your trip
        </Typography>
      </Grid>
      <Grid
        item
        container
        justify="center"
        spacing={2}
        className='resetContainerWithSpacing'
      >
        <Grid
          item
          xs={12}
          sm={6}
          md={3}
        >
          <Autocomplete
            value={startAddress}
            onChange={(e, value, reason) => {
              if (reason === 'select-option') setStartAddress(value);
              if (reason === 'clear') setStartAddress('');
            }}
            options={startSearch}
            renderInput={(params) => (
              <TextField
                variant="outlined"
                {...params}
                required
                label="Start point"
                value={startAddress}
                onChange={(e) => setStartAddress(e.target.value)}
              />
            )}
            renderOption={(option) => (
              <Grid container alignItems="center">
                <Grid item>
                  <LocationOnIcon color='secondary' />
                </Grid>
                {option}
              </Grid>
            )}
          />
        </Grid>
        <Grid
          item
          xs={12}
          sm={6}
          md={3}
        >
          <Autocomplete
            value={endAddress}
            onChange={(e, value, reason) => {
              if (reason === 'select-option') setEndAddress(value);
              if (reason === 'clear') setEndAddress('');
            }}
            options={endSearch}
            renderInput={(params) => (
              <TextField
                variant="outlined"
                {...params}
                required
                label="End point"
                value={endAddress}
                onChange={(e) => setEndAddress(e.target.value)}
              />
            )}
            renderOption={(option) => (
              <Grid container alignItems="center">
                <Grid item>
                  <LocationOnIcon color='secondary' />
                </Grid>
                {option}
              </Grid>
            )}
          />
        </Grid>
      </Grid>
      <Grid
        item
        className={classes.mapContainer}
      >
        <BingMapsReact
          bingMapsKey={bingMapsService.BING_KEY}
          viewOptions={{
            center: { latitude: mapCenter[0], longitude: mapCenter[1] },
          }}
          pushPinsWithInfoboxes={pins}
        />
      </Grid>
    </Grid>
  );
};

TripStartEndPoint.propTypes = {
  startAddress: PropTypes.string.isRequired,
  endAddress: PropTypes.string.isRequired,
  setStartAddress: PropTypes.func.isRequired,
  setEndAddress: PropTypes.func.isRequired,
  setStartPoint: PropTypes.func.isRequired,
  setEndPoint: PropTypes.func.isRequired,
  setIsStartPointValid: PropTypes.func.isRequired,
  setIsEndPointValid: PropTypes.func.isRequired,
};
export default TripStartEndPoint;

import { PieChart } from 'react-minimal-pie-chart';
import PropTypes from 'prop-types';

import { Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';

const PieChartArtists = ({
  data, labelChart, handleFilter
}) => (
  <>
    <Typography
      gutterBottom
      align="center"
      variant="inherit"
      component="h4"
    >
      {labelChart}
    </Typography>
    {!data[0]?.value
      ? (
        <Skeleton variant="circle" width={300} height={300} />)
      : (
        <PieChart
          lineWidth={65}
          paddingAngle={0}
          data={data}
          label={({ dataEntry }) => dataEntry.value}
          labelStyle={{
            fontSize: '4px',
            fontFamily: 'sans-serif',
          }}
          labelPosition={95}
          onClick={(e, segmentIndex) => handleFilter({ artistId: data[segmentIndex].artistId })}
        />
      )}

  </>
);

PieChartArtists.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    artistId: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
  })).isRequired,
  handleFilter: PropTypes.func.isRequired,
  labelChart: PropTypes.string.isRequired,
};

export default PieChartArtists;

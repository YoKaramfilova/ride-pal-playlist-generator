import Skeleton from '@material-ui/lab/Skeleton';
import Grid from '@material-ui/core/Grid';

const SingleTrackSkeleton = () => (
  <Grid
    item
    container
    spacing={2}
    justify="flex-end"
    alignItems="center"
  >
    <Grid item>
      <Skeleton height={60} width={400} />
    </Grid>
    <Grid item>
      <Skeleton variant="circle" height={50} width={50} />
    </Grid>
    <Grid item>
      <Skeleton height={60} width={400} />
    </Grid>
  </Grid>
);
export default SingleTrackSkeleton;

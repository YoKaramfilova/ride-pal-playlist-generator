import React, { useState } from 'react';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import {
  IconButton, Tooltip, Avatar, Grid
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Player from 'pages/Playlist/Player';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import PropTypes from 'prop-types';
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles(() => ({
  paper: {
    padding: '6px 16px',
  },
  center: {
    alignSelf: 'center',
  },
  noOppositeContent: {
    '&:before': {
      content: 'none',
    }
  },
  iconButton: {
    padding: 8,
  },
}));

const PlaylistTrack = ({
  trackName, album, index,
  artist, genre, deezerId, cover,
}) => {
  const classes = useStyles();
  const [player, setPlayer] = useState(false);

  const togglePlayer = () => {
    setPlayer(!player);
  };

  const closePlayer = () => {
    setPlayer(false);
  };

  return (
    <>
      <TimelineItem>
        <TimelineOppositeContent>
          {!deezerId ? (
            <>
              <Skeleton height={60} width={400} />
              <Skeleton variant="circle" height={50} width={50} />
              <Skeleton height={60} width={400} />
            </>
          ) : (
            <>
              <Grid
                container
                alignItems="center"
              >
                {(index % 2 === 0) ? (
                  <Grid
                    item
                    container
                    spacing={2}
                    justify="flex-end"
                    alignItems="center"
                  >
                    <Grid item>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                      >
                        {`Album: ${album}`}
                      </Typography>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                      >
                        {`Genre: ${genre}`}
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Avatar
                        variant="rounded"
                        alt={trackName}
                        src={cover}
                      />
                    </Grid>
                  </Grid>
                ) : (
                  <Grid
                    item
                    container
                    spacing={2}
                    justify="flex-start"
                    alignItems="center"
                  >
                    <Grid item>
                      <Avatar
                        variant="rounded"
                        alt={trackName}
                        src={cover}
                      />
                    </Grid>
                    <Grid item>
                      <Typography variant="body2" color="textSecondary">
                        {`Album: ${album}`}
                      </Typography>
                      <Typography variant="body2" color="textSecondary">
                        {`Genre: ${genre}`}
                      </Typography>
                    </Grid>
                  </Grid>
                )}
              </Grid>
            </>
          )}

        </TimelineOppositeContent>
        <TimelineSeparator>
          <Tooltip
            title={`Listen ${trackName}`}
            arrow
          >
            <IconButton
              onClick={togglePlayer}
              classes={{ root: classes.iconButton }}
              color="secondary"
            >
              <PlayCircleFilledIcon fontSize='large' />
            </IconButton>
          </Tooltip>
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent>
          <Paper elevation={3} className={classes.paper}>
            <Typography variant="inherit" component="h4">
              {trackName}
            </Typography>
            <Typography>{`by ${artist}`}</Typography>
          </Paper>
        </TimelineContent>
      </TimelineItem>

      { player && (
        <TimelineItem classes={{
          root: classes.center,
          missingOppositeContent: classes.noOppositeContent
        }}
        >
          <Player deezerId={deezerId} closePlayer={closePlayer} />
        </TimelineItem>
      )}
    </>
  );
};

PlaylistTrack.propTypes = {
  artist: PropTypes.string.isRequired,
  genre: PropTypes.string.isRequired,
  deezerId: PropTypes.number.isRequired,
  album: PropTypes.string.isRequired,
  trackName: PropTypes.string.isRequired,
  cover: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
};
export default PlaylistTrack;

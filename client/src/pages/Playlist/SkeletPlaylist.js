import SingleTrackSkeleton from 'pages/Playlist/SingleTrackSkeleton';

const SkeletTracklist = () => (
  <>
    <SingleTrackSkeleton />
    <SingleTrackSkeleton />
    <SingleTrackSkeleton />
    <SingleTrackSkeleton />
    <SingleTrackSkeleton />
    <SingleTrackSkeleton />
    <SingleTrackSkeleton />
    <SingleTrackSkeleton />
    <SingleTrackSkeleton />
    <SingleTrackSkeleton />

  </>
);
export default SkeletTracklist;

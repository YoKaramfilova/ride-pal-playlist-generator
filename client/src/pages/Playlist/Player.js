import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  playerContainer: {
    position: 'relative',
    padding: 12,
  },
  closeButton: {
    position: 'absolute',
    top: 24,
    right: 24,
    backgroundColor: theme.palette.background.default,
  },
  hover: {
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.background.default,
    }
  },
}));

const Player = ({ deezerId, closePlayer }) => {
  const classes = useStyles();

  return (
    <div className={classes.playerContainer}>
      <IconButton
        onClick={closePlayer}
        className={classes.closeButton}
        color='primary'
        size='small'
        classes={{ colorPrimary: classes.hover }}
      >
        <CloseIcon fontSize='small' />
      </IconButton>
      <iframe
        title="deezer-widget"
        src={`https://widget.deezer.com/widget/dark/track/${deezerId}`}
        width="100%"
        height="300"
        frameBorder="0"
        allowtransparency="true"
        allow="encrypted-media; clipboard-write"
      />
    </div>
  );
};
Player.propTypes = {
  deezerId: PropTypes.number.isRequired,
  closePlayer: PropTypes.func.isRequired,
};
export default Player;

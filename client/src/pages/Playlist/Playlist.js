import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Timeline from '@material-ui/lab/Timeline';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Divider from '@material-ui/core/Divider';
import PropTypes from 'prop-types';

import ActionAlert from 'components/ActionAlert/ActionAlert';
import PlaylistTrack from 'pages/Playlist/PlaylistTrack';
import PieChartGenres from 'pages/Playlist/PieChartGenres';
import PieChartArtist from 'pages/Playlist/PieChartArtists';
import PlaylistMainInfo from 'pages/Playlist/PlaylistMainInfo';
import DisableFilter from 'pages/Playlist/DisableFilter';
import utils from 'common/utils.js';
import playlistService from 'services/playlists.js';
import tracksService from 'services/tracks.js';
import SkeletTracklist from 'pages/Playlist/SkeletPlaylist';

const useStyles = makeStyles(() => ({
  paper: {
    padding: 12,
  },
  ulReset: {
    margin: 0,
    padding: 0,
  }
}));

const Playlist = ({ match }) => {
  const classes = useStyles();
  const history = useHistory();
  const { id } = match.params;

  const [playlist, setPlaylist] = useState([]);
  const [tracks, setTracks] = useState([]);
  const [dataGenres, setDataGenres] = useState([]);
  const [dataArtist, setDataArtist] = useState([]);
  const [filter, setFilter] = useState();
  const [duration, setDuration] = useState();
  const [rank, setRank] = useState();

  const [loading, setLoading] = useState(false);
  const [notification, setNotification] = useState({ isOpen: false, type: '', message: '' });
  const handleNotificationClose = () => setNotification({ isOpen: false, type: '', message: '' });

  useEffect(() => {
    setLoading(true);

    playlistService.getPlaylist(id)
      .then((result) => {
        if (result.message) {
          setNotification({
            isOpen: true,
            type: 'error',
            message: result.message,
          });
        } else {
          setPlaylist(result);
        }
      })
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
  }, [id]);

  useEffect(() => {
    if (!loading) {
      setLoading(true);

      tracksService.getTracksByPlaylistId(id, filter)
        .then((result) => {
          setTracks(utils.sortRandom(result.data));
          setDataGenres(utils.setDataPieCharts('genre', 'genreId', result.data));
          setDataArtist(utils.setDataPieCharts('artist', 'artistId', result.data));
          setDuration(result.duration);
          setRank(result.rank);
        })
        .catch(() => history.push('/500'))
        .finally(() => setLoading(false));
    }
  }, [id, filter]);

  const handleFilter = (obj) => {
    setFilter(obj);
  };

  const handleDisableFilter = () => {
    setFilter();
  };

  return (
    <Container
      disableGutters
      maxWidth="lg"
    >
      <Grid
        container
        spacing={2}
        className='resetContainerWithSpacing'
      >
        <PlaylistMainInfo
          {...playlist}
          duration={duration}
          rank={rank}
          setPlaylist={setPlaylist}
          numberOfTracks={tracks.length}
        />
        <Divider style={{ width: '100%' }} />
        <Grid
          item
          xs={12}
          sm={6}
          md={3}
        >
          <Grid
            container
            spacing={3}
            direction="column"
            justify="flex-start"
            alignItems="stretch"
            wrap='nowrap'
          >
            <Grid item>
              <PieChartGenres
                data={dataGenres}
                labelChart="Genres mix"
                handleFilter={handleFilter}
                loading={loading}
              />
            </Grid>
            <Grid item>
              {filter && (
                <DisableFilter
                  handleDisableFilter={handleDisableFilter}
                />
              )}
            </Grid>
            <Grid item>
              <PieChartArtist
                data={dataArtist}
                labelChart="Artists mix"
                handleFilter={handleFilter}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid
          item
          xs={12}
          sm={6}
          md={9}
        >
          {!tracks.length
            ? (
              <>
                <SkeletTracklist />
              </>
            ) : (
              <Timeline align="alternate" classes={{ root: classes.ulReset }}>
                {tracks
              && tracks.map((track, index) => (
                <PlaylistTrack
                  key={track.id}
                  {...track}
                  index={index}
                />
              ))}
              </Timeline>
            )}
        </Grid>
      </Grid>
      <ActionAlert
        notification={notification}
        handleClose={handleNotificationClose}
      />
    </Container>
  );
};

Playlist.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default Playlist;

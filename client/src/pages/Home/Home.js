import { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

import { AuthContext } from 'providers/АuthContext';
import PlaylistsSection from 'pages/Home/PlaylistsSection';
import GenresSection from 'pages/Home/GenresSection';
import ArtistsSection from 'pages/Home/ArtistsSection';

const useStyles = makeStyles((theme) => ({
  cover: {
    backgroundImage: 'url(/images/home-cover.png)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: '100% 100%',
    width: 'calc(100% + 32px)',
    margin: '-16px -16px 0 -16px',
  },
  logo: {
    width: theme.spacing(9),
    height: theme.spacing(9),
  },
  coverTypography: {
    color: 'white',
  },
  bold: {
    fontWeight: 'bold',
  },
}));

const Home = ({ history }) => {
  const classes = useStyles();
  const auth = useContext(AuthContext);

  return (
    <Container
      disableGutters
      maxWidth="xl"
    >
      <Grid
        container
        justify="center"
        spacing={7}
        className='resetContainerWithSpacing'
      >
        <Grid
          item
          className={classes.cover}
        >
          <Grid
            container
            direction='column'
            spacing={6}
            alignItems='center'
            className='resetContainerWithSpacing'
          >
            <Grid item>
              <img
                src='/images/logo-white.png'
                alt="logo"
                className={classes.logo}
              />
            </Grid>
            <Grid item>
              <Typography
                variant="h3"
                align="center"
                className={classes.coverTypography}
              >
                ROCK YOUR TRIP
              </Typography>
            </Grid>
            <Grid item>
              <Typography
                variant="h4"
                align="center"
                className={classes.coverTypography}
              >
                Get the perfect rock playlist for your ride.
              </Typography>
            </Grid>
            <Grid item>
              {!auth.user ? (
                <Button
                  onClick={() => history.push('/register')}
                  color="secondary"
                  variant="contained"
                >
                  Register now
                </Button>
              ) : (
                <Button
                  onClick={() => history.push('/create-playlist')}
                  color="secondary"
                  variant="contained"
                >
                  Create playlist
                </Button>
              )}
            </Grid>
          </Grid>
        </Grid>
        <Grid
          item
          xs={12}
        >
          <PlaylistsSection className={classes.bold} />
        </Grid>
        <Grid
          item
          container
          spacing={4}
          xs={12}
        >
          <GenresSection className={classes.bold} />
        </Grid>
        <Grid
          item
          xs={12}
        >
          <ArtistsSection className={classes.bold} />
        </Grid>
      </Grid>
    </Container>
  );
};

Home.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

export default Home;

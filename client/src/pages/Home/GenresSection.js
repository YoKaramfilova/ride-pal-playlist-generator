import { useState, useEffect } from 'react';
import { useHistory, NavLink } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import PropTypes from 'prop-types';

import genresService from 'services/genres';
import GenreSimple from 'components/Genre/GenreSimple';
import Loading from 'components/Loading/Loading';

const GenresSection = ({ className }) => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [genres, setGenres] = useState([]);

  useEffect(() => {
    setLoading(true);

    genresService.getAllGenres()
      .then((res) => setGenres(res))
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
  }, []);

  return (
    <>
      <Grid
        item
        container
        justify="center"
        xs={12}
      >
        <Typography
          variant="h5"
          color="textPrimary"
          className={className}

        >
          Browse playlists by genre
        </Typography>
      </Grid>
      <Grid
        item
        container
        spacing={8}
        justify="center"
        alignItems="center"
      >
        {loading && <Loading />}
        {genres.map((genre) => (
          <Grid
            item
            key={genre.id}
          >
            <IconButton
              component={NavLink}
              to={`/playlists?genres=${genre.name}`}
            >
              <GenreSimple
                name={genre.name}
                picture={genre.picture}
              />
            </IconButton>
          </Grid>
        ))}
      </Grid>
    </>
  );
};
GenresSection.propTypes = {
  className: PropTypes.string.isRequired,
};
export default GenresSection;

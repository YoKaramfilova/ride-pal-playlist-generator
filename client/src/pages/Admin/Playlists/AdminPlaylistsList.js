import React, { useState, useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import {
  Paper, Container,
} from '@material-ui/core';
import Loading from 'components/Loading/Loading';
import AdminTableRow from 'pages/Admin/Playlists/AdminTableRow';
import AdminTablePagination from 'components/Admin/AdminTablePagination';
import AdminPlaylistsTableHead from 'pages/Admin/Playlists/AdminPlaylistsTableHead';
import { FiltersContext } from 'providers/FiltersContext';
import FiltersBar from 'components/PlaylistsFilters/FiltersBar';
import playlistsService from 'services/playlists';
import AdminTableToolbar from 'components/Admin/AdminTableToolbar';
import utils from 'common/utils';
import constants from 'common/constants';
import ActionAlert from 'components/ActionAlert/ActionAlert';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  tableHead: {
    fontWeight: 'bold',
  }
}));

const AdminPlaylistsList = () => {
  const history = useHistory();
  const classes = useStyles();
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('role');
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const {
    titleFilter,
    genresFilter,
    playtimeFilter,
    topPlaylistsFilter,
    sortBy,
    isFilterApplied,
    resetFiltersContext,
  } = useContext(FiltersContext);

  const [isFiltersBarOpen, setFiltersBarOpen] = useState(false);
  const [isSearchTriggered, setIsSearchTriggered] = useState(false);

  const [loading, setLoading] = useState(false);

  const [rows, setRows] = useState([]);
  const [hasChanges, setHasChanges] = useState(false);
  const [notification, setNotification] = useState({ isOpen: false, type: '', message: '' });
  const handleNotificationClose = () => setNotification({ isOpen: false, type: '', message: '' });

  useEffect(() => {
    resetFiltersContext();
  }, []);

  useEffect(() => {
    setLoading(true);

    const urlParams = new URLSearchParams();
    const queryParams = {};

    if (titleFilter) {
      queryParams.title = titleFilter;
      urlParams.append('title', titleFilter);
    }

    const playtimeQueries = Object
      .keys(playtimeFilter).filter(key => playtimeFilter[key]);

    if (playtimeQueries.length) {
      queryParams.playtime = playtimeQueries;
      urlParams.append('playtime', playtimeQueries);
    }

    if (topPlaylistsFilter) {
      queryParams.topPlaylists = topPlaylistsFilter;
      urlParams.append('topTracks', topPlaylistsFilter);
    }

    const genresQueries = Object
      .keys(genresFilter).filter(key => genresFilter[key]);

    if (genresQueries.length) {
      queryParams.genres = genresQueries;
      urlParams.append('genres', genresQueries);
    }

    if (sortBy) {
      queryParams.sortBy = sortBy;
      urlParams.append('sortBy', sortBy);
    }

    playlistsService.getAllPlaylists(queryParams)
      .then((result) => {
        setRows(result.data);
      })
      .catch(() => history.push('/500'))
      .finally(() => {
        setLoading(false);
        setIsSearchTriggered(false);
      });

    history.push({ search: urlParams.toString() });
  }, [isFilterApplied, isSearchTriggered, hasChanges]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const closeFiltersBar = () => {
    setFiltersBarOpen(false);
  };

  return (
    <Container
      disableGutters
      maxWidth="lg"
    >
      <FiltersBar
        open={isFiltersBarOpen}
        setOpen={setFiltersBarOpen}
        closeFiltersBar={closeFiltersBar}
        setIsSearchTriggered={setIsSearchTriggered}
        setPage={setPage}
        fromAdmin='admin'
      />
      <Paper>
        <AdminTableToolbar
          rows={rows}
          setRows={setRows}
          label="Playlists"
          setFiltersBarOpen={setFiltersBarOpen}
        />
        <TableContainer>
          <Table aria-label="collapsible table">
            <AdminPlaylistsTableHead
              classes={classes}
              numSelected={0}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
              setPage={setPage}
            />
            <TableBody>
              {loading && <Loading />}
              {utils.stableSort(rows, utils.getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => (
                  <AdminTableRow
                    key={row.id}
                    row={row}
                    rows={rows}
                    setRows={setRows}
                    hasChanges={hasChanges}
                    setHasChanges={setHasChanges}
                    setNotification={setNotification}
                  />
                ))}
            </TableBody>
          </Table>
        </TableContainer>
        <AdminTablePagination
          handleChangeRowsPerPage={handleChangeRowsPerPage}
          rowsPerPageOptions={constants.ROWS_PER_PAGE}
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
      <ActionAlert
        notification={notification}
        handleClose={handleNotificationClose}
      />
    </Container>
  );
};
export default AdminPlaylistsList;

import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import playlistService from 'services/playlists.js';
import tracksService from 'services/tracks.js';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { DateTime, Duration } from 'luxon';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import InfoIcon from '@material-ui/icons/Info';
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import Loading from 'components/Loading/Loading';
import ConfirmationDialog from 'components/ConfirmationDialog/ConfirmationDialog';
import EditPlaylist from 'components/EditPlaylist/EditPlaylist';
import utils from 'common/utils.js';

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
  tableHead: {
    fontWeight: 'bold',
  }
});

const AdminTableRow = (props) => {
  const history = useHistory();
  const {
    row, setRows, rows, setHasChanges, hasChanges, setNotification
  } = props;
  const [open, setOpen] = useState(false);
  const classes = useRowStyles();
  const [tracks, setTracks] = useState([]);
  const [loading, setLoading] = useState(false);

  const [playlist, setPlaylist] = useState();

  const [isEditFormOpen, setEditFormOpen] = useState(false);
  const handleEditFormClose = () => setEditFormOpen(false);
  const handleEditFormOpen = () => setEditFormOpen(true);

  const [actionConfirm, setActionConfirm] = useState({
    isOpen: false,
    title: '',
    text: '',
    action: () => { },
  });

  const handleTracks = (e, id) => {
    if (!loading) {
      setOpen(!open);
      if (open === false) {
        setLoading(true);
        tracksService.getTracksByPlaylistId(id)
          .then((result) => {
            setTracks(utils.sortRandom(result.data));
          })
          .catch(() => history.push('/500'))
          .finally(() => setLoading(false));
      }
    }
  };

  const handleEditPlaylist = (event, playlistForEdit) => {
    setPlaylist(playlistForEdit);
    handleEditFormOpen();
  };

  const handleProfile = (event, id, username) => {
    history.push({ pathname: `/profile/${username}`, state: id });
  };
  const handlePlaylist = (event, id) => {
    history.push({ pathname: `/playlists/${id}` });
  };

  useEffect(() => {
    if (playlist) {
      const editedPlaylist = rows.find(r => r.id === playlist.id);
      if (editedPlaylist) {
        if (editedPlaylist.title !== playlist.title) {
          editedPlaylist.title = playlist.title;
          setRows(rows);
          setHasChanges(!hasChanges);
        }
      }
    }
  }, [playlist]);

  const deletePlaylist = (id) => {
    setLoading(true);
    playlistService.deletePlaylist(+id)
      .then((res) => {
        if (res.message) {
          setNotification({
            isOpen: true,
            type: 'error',
            message: res.message,
          });
        } else {
          setRows(rows.filter(p => p.id !== id));
          setNotification({
            isOpen: true,
            type: 'success',
            message: `Successfully deleted playlist with id ${id}!`,
          });
        }
      })
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
  };

  const handleDelete = (event, id) => {
    setActionConfirm(
      {
        isOpen: true,
        title: 'Confirm deletion',
        text: `Would you like to proceed with id ${id} deletion?`,
        action: () => deletePlaylist(id),
      }
    );
  };

  const handleCloseDialog = () => setActionConfirm(
    {
      isOpen: false, title: '', text: '', action: () => { },
    }
  );
  return (
    <>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={(e) => handleTracks(e, row.id)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell className={classes.tableHead} component="th" scope="row">
          {row.title}
        </TableCell>
        <TableCell align="right">{row.playtime && Duration.fromMillis(row.playtime * 1000).toFormat('hh:mm:ss')}</TableCell>
        <TableCell align="right">{row.rank}</TableCell>
        <TableCell align="left">{row.genres.split(',').sort().join(', ')}</TableCell>
        <TableCell align="left">{DateTime.fromISO(row.created_on).toFormat('dd LLL yyyy')}</TableCell>
        <TableCell align="center">
          <Tooltip
            title="Edit playlist"
            arrow
          >
            <IconButton
              className={classes.control}
              edge="start"
              onClick={(event) => handleEditPlaylist(event, row)}
            >
              <CreateIcon color='primary' />
            </IconButton>
          </Tooltip>
        </TableCell>
        <TableCell align="center">
          <Tooltip
            title="Delete playlist"
            arrow
          >
            <IconButton
              className={classes.control}
              edge="start"
              onClick={(event) => handleDelete(event, row.id)}
            >
              <DeleteIcon color='secondary' />
            </IconButton>
          </Tooltip>
        </TableCell>
        <TableCell align="left">{row.username}</TableCell>
        <TableCell align="center">
          <Tooltip
            title="View user"
            arrow
          >
            <IconButton
              className={classes.control}
              edge="start"
              onClick={(event) => handleProfile(event, row.user_id, row.username)}
            >
              <InfoIcon color='primary' />
            </IconButton>
          </Tooltip>
        </TableCell>
        <TableCell align="center">
          <Tooltip
            title="View playlist"
            arrow
          >
            <IconButton
              className={classes.control}
              edge="start"
              onClick={(event) => handlePlaylist(event, row.id)}
            >
              <InfoIcon color='secondary' />
            </IconButton>
          </Tooltip>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={11}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div" color='secondary'>
                Tracks:
                {loading && <Loading />}
                {' '}
                {!loading && tracks && tracks.length}
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell className={classes.tableHead} align="left">Name</TableCell>
                    <TableCell className={classes.tableHead} align="left">Artist</TableCell>
                    <TableCell className={classes.tableHead} align="left">Album</TableCell>
                    <TableCell className={classes.tableHead} align="left">Genre</TableCell>
                    <TableCell className={classes.tableHead} align="left">Rank</TableCell>
                    <TableCell className={classes.tableHead} align="left">Playtime</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {tracks && tracks.length > 0 && tracks.map((trackRow) => (
                    <TableRow key={trackRow.id}>
                      <TableCell className={classes.tableHead} component="th" scope="row">
                        {trackRow.trackName}
                      </TableCell>
                      <TableCell>{trackRow.artist}</TableCell>
                      <TableCell>{trackRow.album}</TableCell>
                      <TableCell align="left">{trackRow.genre}</TableCell>
                      <TableCell align="left">{trackRow.rank}</TableCell>
                      <TableCell align="left">{trackRow.duration && Duration.fromMillis(trackRow.duration * 1000).toFormat('hh:mm:ss')}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
      <ConfirmationDialog
        actionConfirm={actionConfirm}
        handleClose={handleCloseDialog}
      />
      <EditPlaylist
        isOpen={isEditFormOpen}
        handleClose={handleEditFormClose}
        playlist={playlist}
        setPlaylist={setPlaylist}
      />
    </>
  );
};

AdminTableRow.propTypes = {
  row: PropTypes.shape({
    genre: PropTypes.string,
    user_id: PropTypes.number,
    id: PropTypes.number,
    genres: PropTypes.string,
    title: PropTypes.string,
    playtime: PropTypes.number,
    rank: PropTypes.number,
    created_on: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
  }).isRequired,
  rows: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    username: PropTypes.string,
    email: PropTypes.string,
    avatar: PropTypes.string,
    created_on: PropTypes.string,
  }).isRequired).isRequired,
  setRows: PropTypes.func.isRequired,
  setHasChanges: PropTypes.func.isRequired,
  hasChanges: PropTypes.bool.isRequired,
  setNotification: PropTypes.func.isRequired,
};

export default AdminTableRow;

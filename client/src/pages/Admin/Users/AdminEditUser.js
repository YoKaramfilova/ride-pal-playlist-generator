/* eslint-disable camelcase */
import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import {
  TextField, Button, Dialog, DialogTitle, DialogActions,
  DialogContent, Grid, DialogContentText
} from '@material-ui/core';
import _ from 'lodash';
import PropTypes from 'prop-types';

import ActionAlert from 'components/ActionAlert/ActionAlert';
import Loading from 'components/Loading/Loading';
import formControls from 'common/form-controls';
import adminService from 'services/admin';
import utils from 'common/utils';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import ConfirmationDialog from 'components/ConfirmationDialog/ConfirmationDialog';

const AdminEditUser = ({
  isOpen, handleClose, userId, user, setHasChanges, hasChanges
}) => {
  const { username, role_id } = user;
  const history = useHistory();
  const [notification, setNotification] = useState({ isOpen: false, type: '', message: '' });
  const handleNotificationClose = () => setNotification({ isOpen: false, type: '', message: '' });

  const [loading, setLoading] = useState(false);
  const [isFormValid, setIsFormValid] = useState(false);
  const [form, setForm] = useState(_.cloneDeep(formControls.USER_FORM_CONTROLS));
  const [stateAdmin, setStateAdmin] = useState(role_id === 1);

  const [actionConfirm, setActionConfirm] = useState({
    isOpen: false,
    title: '',
    text: '',
    action: () => { },
  });

  const handleCloseDialog = () => setActionConfirm(
    {
      isOpen: false, title: '', text: '', action: () => { },
    }
  );

  const handleChangeAdmin = () => {
    setActionConfirm(
      {
        isOpen: true,
        title: 'Confirm new user status',
        text: `Would you like to proceed with changing user status of id ${userId}?`,
        action: () => {
          setStateAdmin(!stateAdmin);
          adminService.updateUser(userId, { role_id: `${stateAdmin ? 2 : 1}` })
            .then((res) => {
              if (res.message) {
                setNotification({
                  isOpen: true,
                  type: 'error',
                  message: res.message,
                });
              } else if (res.errors) {
                const message = Object
                  .entries(res.errors)
                  .map(error => `${error[0]}: ${error[1]}`)
                  .join('\n');

                setNotification({
                  isOpen: true,
                  type: 'error',
                  message,
                });
              } else {
                setNotification({
                  isOpen: true,
                  type: 'success',
                  message: 'Successfully updated user profile.'
                });
              }

              setHasChanges(!hasChanges);
              handleClose();
            });
        },
      }
    );
  };

  useEffect(() => {
    setStateAdmin(role_id === 1);
  }, [userId]);

  const formSubmitHandler = (e) => {
    e.preventDefault();

    const body = {};

    Object.entries(form).forEach(item => {
      const [key, controls] = item;
      if (controls.value) {
        body[key] = controls.value;
      }
    });

    setLoading(true);

    adminService.updateUser(userId, body)
      .then((res) => {
        if (res.message) {
          setNotification({
            isOpen: true,
            type: 'error',
            message: res.message,
          });
        } else if (res.errors) {
          const message = Object
            .entries(res.errors)
            .map(error => `${error[0]}: ${error[1]}`)
            .join('\n');

          setNotification({
            isOpen: true,
            type: 'error',
            message,
          });
        } else {
          setNotification({
            isOpen: true,
            type: 'success',
            message: 'Successfully updated user profile.'
          });
        }
        setHasChanges(!hasChanges);
        setForm(_.cloneDeep(formControls.USER_FORM_CONTROLS));
      })
      .catch(() => history.push('/500'))
      .finally(() => setLoading(false));
    handleClose();
  };

  return (
    <>
      <Dialog open={isOpen} onClose={handleClose}>
        <DialogTitle>
          Edit user
          {' '}
          {username}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Edit some or all of user details
          </DialogContentText>
          <Grid container spacing={2} component='form'>
            <Grid item>
              <FormControlLabel
                control={<Switch checked={stateAdmin} onChange={handleChangeAdmin} />}
                label="ADMIN"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                name={form.username.name}
                placeholder={form.username.placeholder}
                value={form.username.value}
                variant="outlined"
                error={form.username.touched && !form.username.valid}
                multiline
                required
                fullWidth
                label={form.username.touched
                  && !form.username.valid
                  ? 'Error'
                  : ''}
                helperText={form.username.touched
                  && !form.username.valid
                  ? `Username should be between 8-20 characters and include
              alphanumeric characters, _ and . (non-consecutive).`
                  : ''}
                onChange={(e) => {
                  utils.handleInputChange(e, form, setForm, setIsFormValid);
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                name={form.email.name}
                placeholder={form.email.placeholder}
                value={form.email.value}
                variant="outlined"
                error={form.email.touched && !form.email.valid}
                multiline
                required
                fullWidth
                label={form.email.touched
                  && !form.email.valid
                  ? 'Error'
                  : ''}
                helperText={form.email.touched
                  && !form.email.valid
                  ? 'Expected email to be valid email with length of at least 7 symbols.'
                  : ''}
                onChange={(e) => {
                  utils.handleInputChange(e, form, setForm, setIsFormValid);
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                name={form.password.name}
                placeholder={form.password.placeholder}
                value={form.password.value}
                type="password"
                variant="outlined"
                error={form.password.touched && !form.password.valid}
                multiline
                required
                fullWidth
                label={form.password.touched
                  && !form.password.valid
                  ? 'Error'
                  : ''}
                helperText={form.password.touched
                  && !form.password.valid
                  ? `Password should be with length [8-20] and contain at least one
              number, one lowercase letter, one capital letter and one special symbol.`
                  : ''}
                onChange={(e) => {
                  utils.handleInputChange(e, form, setForm, setIsFormValid);
                }}
              />
            </Grid>
          </Grid>
          {loading && <Loading />}
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            disabled={!isFormValid || !Object.values(form).some(
              (control) => control.touched
            )}
            color="secondary"
            onClick={(e) => formSubmitHandler(e)}
          >
            Submit
          </Button>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
      <ActionAlert
        notification={notification}
        handleClose={handleNotificationClose}
      />
      <ConfirmationDialog
        actionConfirm={actionConfirm}
        handleClose={handleCloseDialog}
      />
    </>
  );
};

AdminEditUser.propTypes = {
  user: PropTypes.shape({
    role_id: PropTypes.number,
    username: PropTypes.string,
  }),
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  userId: PropTypes.number,
  hasChanges: PropTypes.bool.isRequired,
  setHasChanges: PropTypes.func.isRequired,
};

AdminEditUser.defaultProps = {
  user: PropTypes.shape({
    role_id: undefined,
    username: undefined,
  }),
  userId: undefined,
};

export default AdminEditUser;

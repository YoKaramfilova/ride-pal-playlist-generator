/* eslint-disable camelcase */
import React from 'react';
import AdminTableHead from 'components/Admin/AdminTableHead';
import PropTypes from 'prop-types';

const headCells = [
  {
    id: 'id', numeric: true, icon: false, disablePadding: true, label: 'ID'
  },
  {
    id: 'username', numeric: false, icon: false, disablePadding: false, label: 'Username'
  },
  {
    id: 'role', numeric: false, icon: false, disablePadding: false, label: 'Type'
  },
  {
    id: 'email', numeric: false, icon: false, disablePadding: false, label: 'Contact'
  },
  {
    id: 'created_on', numeric: false, icon: false, disablePadding: false, label: 'Member since'
  },
  {
    id: 'numberOfPlaylists', numeric: true, icon: false, disablePadding: false, label: 'Number of Playlists'
  },
  {
    id: 'editUser', numeric: false, icon: true, disablePadding: false, label: 'Edit User'
  },
  {
    id: 'infoUser', numeric: false, icon: true, disablePadding: false, label: 'User Info'
  },
];

const AdminUsersTableHead = (
  {
    classes, order, orderBy, numSelected, rowCount, onRequestSort
  }
) => (
  <AdminTableHead
    classes={classes}
    order={order}
    orderBy={orderBy}
    numSelected={numSelected}
    rowCount={rowCount}
    headCells={headCells}
    onRequestSort={onRequestSort}
  />

);

AdminUsersTableHead.propTypes = {
  classes: PropTypes.objectOf.isRequired,
  numSelected: PropTypes.number,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};
AdminUsersTableHead.defaultProps = {
  numSelected: undefined,
};
export default AdminUsersTableHead;
